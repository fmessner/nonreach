{-# LANGUAGE TemplateHaskell #-}

module Main
  ( main,
  )
where

import ArgParser
import qualified Data.Nonreach as Nonreach
import qualified Data.Nonreach.Config as Cfg
import Data.Semigroup ((<>))
import Data.Version (showVersion)
import Development.GitRev
  ( gitBranch,
    gitDirty,
    gitHash,
  )
import Options.Applicative
import Paths_nonreach (version)
import System.IO
import System.Timeout (timeout)

printResult :: Int -> String -> IO ()
printResult to r = do
  p <- timeout to $ putStrLn r
  maybe (putStrLn "TIMEOUT") return p

processInput :: Input -> IO ()
processInput i = do
  (s, f) <- Nonreach.runIO (config i) (trs i) (problems i)
  mapM_
    ( \y -> case y of
        Left e -> putStrLn $ "ERROR: " ++ e
        Right (m, r) ->
          printResult
            (Cfg.timeout $ config i)
            ( Nonreach.formatOutput
                (Cfg.output . config $ i)
                (Nonreach.changeFormat m f, r)
            )
    )
    s

processVI :: VersionInfo -> IO ()
processVI Version = putStrLn ("nonreach " ++ showVersion version)
processVI Info =
  putStrLn
    ( concat
        [ "nonreach ",
          showVersion version,
          " [git: ",
          $(gitBranch),
          "@",
          $(gitHash),
          if $(gitDirty) then " (uncommitted files present) ]" else " ]"
        ]
    )

main :: IO ()
main = do
  hSetBuffering stdout LineBuffering
  input <- execParser opts
  case input of
    Left i -> processInput i
    Right vi -> processVI vi
  where
    opts =
      info
        (input <**> helper)
        ( fullDesc
            <> progDesc
              "Check several reachability problems for a TRS. Give problems in the following syntax: source_term -> target_term"
            <> header
              ( "nonreach (version "
                  ++ showVersion version
                  ++ ") - a tool for reachability analysis in term rewriting"
              )
        )
