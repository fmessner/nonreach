module ArgParser
  ( Input (..),
    input,
    VersionInfo (..),
  )
where

import Data.Nonreach.Config
  ( Check (..),
    Configuration (..),
    Decomposition (..),
    Format (..),
    Problems (..),
    Trs (..),
  )
import qualified Data.Nonreach.Config as C
import Data.Semigroup ((<>))
import Options.Applicative

data OutputDefault = Full | ResultOnly | Certificate deriving (Show, Read)

fromOutputDefault :: OutputDefault -> C.Output
fromOutputDefault ResultOnly = [C.Result]
fromOutputDefault Certificate = [C.Result, C.Certificate]
fromOutputDefault Full =
  [ C.Result,
    C.DroppedRules,
    C.InlinedRules,
    C.LeftInlinedRules,
    C.InfeasibleRules,
    C.RedundantRules,
    C.HumanReadable
  ]

fileInput :: Parser Trs
fileInput =
  TrsFile
    <$> strOption
      (long "file" <> short 'f' <> metavar "FILENAME" <> help "TRS input file")

literalInput :: Parser Trs
literalInput =
  TrsLiteral
    <$> strOption
      ( long "literal"
          <> short 'd'
          <> metavar "LITERAL"
          <> help
            "TRS as command line argument"
      )

trsInput :: Parser Trs
trsInput = fileInput <|> literalInput

fileProblem :: Parser Problems
fileProblem =
  ProblemFile
    <$> strOption
      ( long "problemfile"
          <> short 'p'
          <> metavar "PFILENAME"
          <> help
            "Problem input file"
      )

literalProblems :: Parser Problems
literalProblems =
  ProblemLiteral
    <$> strOption
      ( long "problems"
          <> short 'P'
          <> metavar "PLITERAL"
          <> help
            "Problems as command line argument"
      )

problemInput :: Parser (Maybe Problems)
problemInput = optional (fileProblem <|> literalProblems)

-- data Input = Input { trs :: TrsInput, strategy :: [Strategy],
--   problems :: Maybe ProblemInput, leftNarrow :: Int, rightNarrow :: Int,
--   parallel :: Bool, to :: Int, outputFormat :: Maybe OutputFormat}

decompInput :: Parser [Decomposition]
decompInput =
  option
    auto
    ( long "decomposition"
        <> short 'D'
        <> help "Decomposition strategies to be used (like: NarrowLeft n)."
        <> value [NarrowLeft 2]
        <> showDefault
    )

-- NOTE old format:
-- NarrowLeft <$> option
-- auto
-- (  long "left-narrowing"
-- <> short 'l'
-- <> help "How many left-narrowing steps will be applied."
-- <> value 2
-- <> showDefault
-- )

outputInput :: Parser C.Output
outputInput = left <|> right
  where
    left =
      option
        auto
        ( long "output"
            <> short 'o'
            <> help "Specify output as Haskell List."
            <> showDefault
        )
    right =
      fromOutputDefault
        <$> option
          auto
          ( long "output-simple"
              <> help
                "Specify output as one of the following: Full, Certificate, ResultOnly"
              <> value Full
              <> showDefault
          )

cfgInput :: Parser Configuration
cfgInput =
  Configuration
    <$> option
      auto
      ( long "strategy"
          <> short 's'
          <> help "Which checks to use in what order."
          <> showDefault
          <> value [STG, TCAP]
          <> metavar "STRATEGY"
      )
    <*> decompInput
    -- TODO maybe re-allow parallelism
    -- <*> switch (long "parallel" <> help "Check problems in parallel.")

    <*> option
      auto
      ( long "timeout"
          <> short 't'
          <> help "Timeout per problem (microseconds)."
          <> value (-1)
          <> showDefault
      )
    <*> optional (option auto (long "mode" <> short 'm'))
    <*> switch
      (long "redundant-rules" <> help "Use Redundant-Rules preprocessing.")
    <*> outputInput

data Input = Input {trs :: Trs, problems :: Maybe Problems, config :: Configuration}

data VersionInfo = Version | Info

versionInfoParser :: Parser VersionInfo
versionInfoParser = version <|> info
  where
    version =
      flag' Version (long "version" <> short 'v' <> help "Output version number.")
    info = flag' Info (long "info" <> help "Output version number and git hash.")

input :: Parser (Either Input VersionInfo)
input = left <|> right
  where
    left = Left <$> (Input <$> trsInput <*> problemInput <*> cfgInput)
    right = Right <$> versionInfoParser
