# nonreach
*nonreach* is a tool for nonreachability analysis in term rewriting, which I initially wrote for my bachelor thesis at the University of Innsbruck.

## Installation
 * install the [Haskell Tool Stack](https://docs.haskellstack.org/en/stable/README/)
 * clone this repository
 * locate the project directory containing the *stack.yaml* file
 * run `stack build` and / or `stack install`

## Usage
Run `nonreach` or `nonreach --help` to find out about parameters.

Most importantly, input term rewrite systems need to be given in the (old)
format[1] of the [Termination Problem Data
Base](http://cl2-informatik.uibk.ac.at/mercurial.cgi/TPDB) and input problems
are given simply as `s -> t` (multiple problems divided by whitespace).

---
[1] https://www.lri.fr/~marche/tpdb/format.html
