module Data.Nonreach
  ( runIO,
    run,
    formatOutput,
    changeFormat,
    ShowPrettyString,
    ResultType,
  )
where

import Data.Either (partitionEithers)
import Data.List as List
import qualified Data.Map.Strict as M
import Data.Maybe
  ( fromJust,
    fromMaybe,
    isJust,
  )
import qualified Data.Nonreach.BoolStruct as BS
import qualified Data.Nonreach.ConditionalRewriting as C
import Data.Nonreach.Config
import Data.Nonreach.Decomposition
  ( decomp,
    narrowLeft,
  )
import Data.Nonreach.Problem
  ( Problem (..),
    ProblemType,
    atom,
    prettyProblem,
  )
import qualified Data.Nonreach.Problem as Problem
import Data.Nonreach.ProofTree
  ( ProofTree (..),
    prettyProof,
  )
import qualified Data.Nonreach.ProofTree as PT
import Data.Nonreach.Reachability
  ( Result (..),
    exploit,
    isNo,
    isReachable,
    isYes,
    simplify,
  )
import Data.Nonreach.RedundantRules (redundantRules)
import Data.Nonreach.Substitution
import qualified Data.Nonreach.SymbolTransitionGraph as STG
import qualified Data.Nonreach.Tcap as TCAP
import Data.Nonreach.Term
  ( VType,
    renameVType,
    renamerNarrow,
  )
import Data.Nonreach.Utils
  ( ShowPrettyString (..),
    nfold,
  )
import Data.Nonreach.Xml
  ( Proof (..),
    generateXmlCert,
    proofFromResult,
  )
import Data.Rewriting.Substitution (prettySubst)
import Data.Rewriting.Substitution.Type
  ( Subst,
    fromMap,
    toMap,
  )
import Data.Rewriting.Term
  ( Term (..),
    prettyTerm,
  )
import Text.PrettyPrint.ANSI.Leijen (Doc)
import qualified Text.PrettyPrint.ANSI.Leijen as Doc

type ResultType f v = (Meta f v, Result f v)

prettyStringVar :: (ShowPrettyString v) => VType v -> String
prettyStringVar (_, _, "s", x) = showPretty x
prettyStringVar (_, _, "t", x) = showPretty x
prettyStringVar (_, _, "l", x) = showPretty x
prettyStringVar (_, _, "r", x) = showPretty x
-- TODO clean this case up

prettyStringVar (a, b, s, x) =
  showPretty x ++ "_" ++ s ++ "_" ++ show a ++ "_" ++ show b

formatProof :: (f -> Doc) -> (v -> Doc) -> ProofTree f v -> Doc
formatProof f v pt = prettyProof f v pt (Doc.text "INFEASIBLE because")

processFeasibilityProof ::
  (Ord v) =>
  (f -> Doc) ->
  (VType v -> Doc) ->
  Substitution f (VType v) ->
  Doc
processFeasibilityProof f v s =
  (Doc.text "FEASIBLE with" Doc.<$> prettySubst f v (fromMap s'))
  where
    s' = M.filterWithKey keepOriginalVarsOnly (toMap $ toSubst s)
    keepOriginalVarsOnly (_, _, "s", _) _ = True
    keepOriginalVarsOnly (_, _, "t", _) _ = True
    keepOriginalVarsOnly (_, _, _, _) _ = False

data Meta f v = Meta
  { yesNoFormat :: Format,
    redundantRulesResult ::
      Maybe ([C.ConditionalRule f v], [C.ConditionalRule f v]),
    computedISTG :: Maybe (STG.Graph f),
    infeasibleRulesResult ::
      Maybe ([C.ConditionalRule f v], [C.ConditionalRule f v]),
    droppedRules :: Maybe [C.ConditionalRule f v],
    inlinedRules ::
      Maybe
        ( [C.ConditionalRule f v],
          [(C.ConditionalRule f v, (C.ConditionalRule f v, [C.Condition f v]))]
        ),
    leftInlinedRules ::
      Maybe
        ( [C.ConditionalRule f v],
          [(C.ConditionalRule f v, (C.ConditionalRule f v, [C.Condition f v]))]
        ),
    initialRules :: [C.ConditionalRule f v],
    givenProblems :: [Problem f v],
    freshSymbol :: f
  }

changeFormat :: Meta f v -> Format -> Meta f v
changeFormat m f = m {yesNoFormat = f}

-- TODO move out
prettyRules :: (f -> Doc) -> (v -> Doc) -> [C.ConditionalRule f v] -> Doc
prettyRules f v cs = Doc.vsep $ map (prettyRule f v) cs
  where
    prettyRule f v (C.ConditionalRule l r c) =
      prettyTerm f v l
        Doc.<+> Doc.text "->"
        Doc.<+> prettyTerm f v r
        Doc.<+> Doc.softbreak
        Doc.<+> Doc.text "|"
        Doc.<+> Doc.sep (map (prettyProblem f v) c)

prettyOutput ::
  (Ord v, ShowPrettyString f, ShowPrettyString v, Read f, Read v) => -- TODO evaluate these
  (f -> Doc) ->
  (VType v -> Doc) ->
  Output ->
  ResultType f (VType v) ->
  Doc
prettyOutput f v o (m, r) = Doc.vsep $ map go o
  where
    go Result = formatResult (yesNoFormat m) r
    go NL = Doc.hardline
    go RedundantRules =
      maybe
        (Doc.text "Did not use redundant rules method.")
        ( \(remain, removed) ->
            Doc.text "Removed "
              Doc.<> Doc.int (length removed)
              Doc.<> Doc.text " redundant rules"
              Doc.<> if (length removed > 0)
                then
                  Doc.colon
                    Doc.<$> Doc.indent 2 (prettyRules f v removed)
                    Doc.<$> Doc.text "Keeping:"
                    Doc.<$> Doc.indent 2 (prettyRules f v remain)
                else Doc.dot
        )
        (redundantRulesResult m)
    go InlinedRules =
      maybe
        (Doc.text "Did not inline conditions.")
        ( \inlined ->
            Doc.text "Inlined conditions for "
              Doc.<> Doc.int (length . snd $ inlined)
              Doc.<> Doc.text " rules"
              Doc.<> if ((length . snd) inlined > 0)
                then
                  Doc.colon
                    Doc.<$> Doc.indent
                      2
                      (prettyRules f v (map fst . snd $ inlined))
                else Doc.dot
        )
        (inlinedRules m)
    go LeftInlinedRules =
      maybe
        (Doc.text "Did not use left-inlining.")
        ( \inlined ->
            Doc.text "Left-inlined conditions for "
              Doc.<> Doc.int (length . snd $ inlined)
              Doc.<> Doc.text " rules"
              Doc.<> if ((length . snd) inlined > 0)
                then
                  Doc.colon
                    Doc.<$> Doc.indent
                      2
                      (prettyRules f v (map fst . snd $ inlined))
                else Doc.dot
        )
        (leftInlinedRules m)
    go InfeasibleRules =
      maybe
        (Doc.text "Did not use Ifrit-method.")
        ( \(_, infeasible) ->
            Doc.text "Due to Ifrit-method removed "
              Doc.<> Doc.int (length infeasible)
              Doc.<> Doc.text " infeasible rules"
              Doc.<> if (length infeasible > 0)
                then
                  Doc.colon
                    Doc.<$> Doc.indent 2 (prettyRules f v infeasible)
                else Doc.dot
        )
        (infeasibleRulesResult m)
    go DroppedRules =
      maybe
        (Doc.text "Did not drop rules.")
        ( \dropped ->
            Doc.text "Due to conditions equal to the given problem, dropped "
              Doc.<> Doc.int (length dropped)
              Doc.<> Doc.text " rules"
              Doc.<> if (length dropped > 0)
                then Doc.colon Doc.<$> Doc.indent 2 (prettyRules f v dropped)
                else Doc.dot
        )
        (droppedRules m)
    go HumanReadable = formatResultProof f v r
    go Certificate =
      if isNo r
        then
          Doc.text
            ( generateXmlCert
                (map C.renamerDrop $ initialRules m)
                (map Problem.renamerDrop $ givenProblems m)
                ( ( \p -> case (inlinedRules m) of
                      Nothing -> p
                      Just (rs, inlined) ->
                        if null inlined
                          then p
                          else
                            RightInline
                              ( map C.renamerDrop rs,
                                map
                                  (\(x, (y, z)) -> (C.renamerDrop y, map Problem.renamerDrop z))
                                  inlined
                              )
                              p
                  )
                    . ( \p -> case (leftInlinedRules m) of
                          Nothing -> p
                          Just (rs, inlined) ->
                            if null inlined
                              then p
                              else
                                LeftInline
                                  ( map C.renamerDrop rs,
                                    map
                                      ( \(x, (y, z)) ->
                                          (C.renamerDrop y, map Problem.renamerDrop z)
                                      )
                                      inlined
                                  )
                                  p
                      )
                    . ( \p -> case (infeasibleRulesResult m) of
                          Nothing -> p
                          Just (feasible, inf) ->
                            if null inf then p else Ifrit (map C.renamerDrop $ feasible) p
                      )
                    $ proofFromResult
                      (freshSymbol m)
                      ((\(NO pt) -> PT.rename (\(_, _, _, x) -> x) pt) r)
                )
            )
        else Doc.empty

formatOutput ::
  (Ord v, ShowPrettyString f, ShowPrettyString v, Read f, Read v) =>
  Output ->
  ResultType f (VType v) ->
  String
formatOutput o (m, r) =
  show $
    prettyOutput
      (Doc.text . showPretty)
      (Doc.text . showPretty . prettyStringVar)
      o
      (m, r)
{-# INLINEABLE formatOutput #-}

formatResult :: Format -> Result f (VType v) -> Doc
formatResult Nonreachability (YES _) = Doc.text "YES"
formatResult Nonreachability (NO _) = Doc.text "NO"
formatResult Infeasibility (YES _) = Doc.text "NO"
formatResult Infeasibility (NO _) = Doc.text "YES"
formatResult _ MAYBE = Doc.text "MAYBE"
{-# INLINEABLE formatResult #-}

formatResultProof ::
  (Ord v) => (f -> Doc) -> (VType v -> Doc) -> Result f (VType v) -> Doc
formatResultProof f v (YES s) = processFeasibilityProof f v s
formatResultProof f v (NO pt) = formatProof f v pt
formatResultProof _ _ _ = Doc.empty
{-# INLINEABLE formatResultProof #-}

runIO ::
  Configuration ->
  Trs ->
  Maybe Problems ->
  IO ([Either String (ResultType String (VType String))], Format)
runIO c term probs = do
  ctrs <- parseCtrsInput term
  let vs = C.variables ctrs
  ps <- getProbs ctrs vs
  case ps of
    Nothing -> do
      i <- getContents
      let ls = lines i
      return
        ( concatMap
            ( either
                (\x -> [Left $ show x])
                (\xs -> map Right $ run (Config ctrs xs f freshF c))
                . either (Left) (\x -> Right [x])
                . Problem.fromString vs
            )
            ls,
          f
        )
      where
        f = fromMaybe Nonreachability $ fmt c
    Just (ps', f') ->
      return
        ( either
            (\x -> [Left $ show x])
            (\x -> map Right $ run (Config ctrs x f freshF c))
            ps',
          f
        )
      where
        f = fromMaybe f' $ fmt c
  where
    freshF = "|" -- this is not allowed as a function symbol due to the parser
    getProblems (ProblemFile f) vs = Problem.fromFile vs f
    getProblems (ProblemLiteral l) vs = return $ Problem.fromString vs l
    getProbs ctrs vs = case C.problems ctrs of
      Nothing -> case probs of
        -- solve probs from stdin
        Nothing -> return Nothing
        -- solve probs given as file or literal
        Just pIn -> do
          p <- getProblems pIn vs
          return $ Just (either Left (Right . map (\x -> [x])) p, Nonreachability)
      -- solve probs given in CTRS
      -- TODO drop rules with given conditions

      Just cs -> return $ Just (Right [cs], Infeasibility)

--       -- drop rules with conditions equal to the ones we are checking

--                      (filter (not . C.equalModuloRenaming cs . C.cond) rs)

--                      (All $ map Atom $ Problem.uniqueVars "c" cs)

--   where rs = map (C.uniqueVars "r") $ C.strictRules $ C.rules c

run :: (Ord f, Ord v) => Config f v -> [ResultType f (VType v)]
run c =
  let ps' = getPs
   in map
        ( \((rs, meta), ps) ->
            let res = runOne (rs, atom $ problemFromConj ps) in (meta, res)
        )
        (zip (map getRs ps') ps')
  where
    problemFromConj ps =
      Reachability (Fun (fresh c) (map src ps)) (Fun (fresh c) (map tgt ps))
    problemAtoms = map atom
    problemRenamed =
      map
        ( map
            ( Problem.renamerInitial
                "s"
                ( case format c of
                    Infeasibility -> "s"
                    Nonreachability -> "t"
                )
            )
        )
    baseMeta =
      ( Meta
          (format c)
          Nothing
          Nothing
          Nothing
          Nothing
          Nothing
          Nothing
          []
          []
          (fresh c)
      )
    getPs = problemRenamed $ problems c
    getRs ps =
      rsRedundancy
        . rsSTGSimp
        . rsLeftInlined
        . rsInlined
        -- . rsFiltered
        . rsRenamed
        $ baseRules
      where
        baseRules =
          let r = C.strictRules . C.rules $ trs c
           in (r, baseMeta {initialRules = r, givenProblems = []})
        rsRenamed (r, m) =
          ( rs,
            m
              { redundantRulesResult =
                  maybe
                    Nothing
                    (\(x, y) -> Just (renamer x, renamer y))
                    (redundantRulesResult m),
                infeasibleRulesResult =
                  maybe
                    Nothing
                    (\(x, y) -> Just (renamer x, renamer y))
                    (infeasibleRulesResult m),
                droppedRules = maybe Nothing (Just . renamer) (droppedRules m),
                inlinedRules =
                  maybe
                    Nothing
                    ( Just . \(x, y) ->
                        ( renamer x,
                          map
                            ( \(r, (r', cs)) ->
                                ( C.renamerInitial "r" r,
                                  ( C.renamerInitial "r" r',
                                    map (Problem.renamerInitial "c" "c") cs
                                  )
                                )
                            )
                            y
                        )
                    )
                    (inlinedRules m),
                leftInlinedRules =
                  maybe
                    Nothing
                    ( Just . \(x, y) ->
                        ( renamer x,
                          map
                            ( \(r, (r', cs)) ->
                                ( C.renamerInitial "r" r,
                                  ( C.renamerInitial "r" r',
                                    map (Problem.renamerInitial "c" "c") cs
                                  )
                                )
                            )
                            y
                        )
                    )
                    (leftInlinedRules m),
                initialRules = renamer (initialRules m),
                givenProblems = ps,
                freshSymbol = fresh c
              }
          ) -- FIXME this potentially  overrides previous metadata
          where
            renamer = map (C.renamerInitial "r")
            rs = renamer r
        rsSTGSimp (r, m) =
          if elem STG (checks $ cfg c)
            then
              let (g, r') = STG.graphFromRules [TCAP.reach] r
               in ( r',
                    m
                      { infeasibleRulesResult = Just (r', r List.\\ r'),
                        computedISTG = Just g
                      }
                  )
            else (r, m)
        rsFiltered (r, m) =
          let (r', dropped) = C.filterOutCond ps r
           in (r', m {droppedRules = Just dropped}) -- TODO
        rsInlined (r, m) =
          let (r', inlined) = C.inlineConditions r
           in (r', m {inlinedRules = Just (r', inlined)}) -- TODO
        rsLeftInlined (r, m) =
          let (r', inlined) = C.leftInlineConditions r
           in (r', m {leftInlinedRules = Just (r', inlined)}) -- TODO
        rsRedundancy (r, m) =
          if redundantRulesCheck . cfg $ c
            then
              let (rs, removed) = redundantRules rrReach r
               in (rs, m {redundantRulesResult = Just (rs, removed)})
            else (r, m)

    rrReach rs = isReachable cs . simplify cs . decompose rsSimp cs rootCs
      where
        (cs, rootCs, rsSimp) = getChecks rs
    getChecks rs = (map getCheck (checks $ cfg c), rootChecks stg rsSimp, rsSimp)
      where
        (stg, rsSimp) = STG.graphFromRules [TCAP.reach] rs
        getCheck TCAP = TCAP.reach rsSimp
        getCheck STG = STG.reach stg
    runOne (rs, ps) =
      isReachable cs . simplify cs . decompose rsSimp cs rootCs $ ps
      where
        (cs, rootCs, rsSimp) = getChecks rs
    rootChecks stg rsSimp = [STG.rootReachable stg, TCAP.rootReachable rsSimp]
    decompose rs cs rootCs =
      (foldr ((.) . getDecomp rs cs) id $ decomps $ cfg c) . f cs rootCs
      where
        f cs rootCs = simplify cs . exploit . decomp rootCs . simplify cs
        getDecomp rs cs (NarrowLeft n) =
          nfold
            (\i -> simplify cs . narrowLeft (renamerNarrow i) rs . f cs rootCs)
            n

-- TODO mke rootChecks dynamic
