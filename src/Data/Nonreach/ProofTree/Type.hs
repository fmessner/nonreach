module Data.Nonreach.ProofTree.Type
  ( ProofTree (..),
    Method (..),
    Source (..),
  )
where

import Data.Nonreach.Problem.Type (Problem (..))
import Data.Rewriting.Substitution (Subst)
import Data.Rewriting.Term (Term (..))

data ProofTree f v
  = Empty
  | Cons (ProofTree f v) (ProofTree f v)
  | Collect (Problem f v) (Source f v) [ProofTree f v]
  | Proof (Source f v) (Method f v)
  | Source (Source f v)
  deriving (Show)

data Method f v
  = Tcap (Problem f v)
  | Stg (Problem f v)
  | Unif (Problem f v)
  | MergeSubst (Subst f v) (Subst f v)
  deriving (Show)

data Source f v
  = Initial
  | ChangeMe
  | LNarrowed
  | RootDecomp
  | Transitivity (Problem f v) (Problem f v)
  | SameLhs (Problem f v) (Problem f v)
  | SameRhs (Problem f v) (Problem f v)
  deriving (Show)
