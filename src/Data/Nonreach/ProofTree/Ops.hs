module Data.Nonreach.ProofTree.Ops
  ( source,
    proofs,
    prettyProof,
    rename,
  )
where

import qualified Data.Nonreach.Problem.Ops as Problem
import Data.Nonreach.Problem.Pretty
import Data.Nonreach.Problem.Type (Problem (..))
import Data.Nonreach.ProofTree.Type
import Data.Nonreach.Substitution (renameSubst)
import Data.Rewriting.Substitution
  ( Subst,
    prettySubst,
  )
import Data.Rewriting.Term
  ( Term (..),
    prettyTerm,
  )
import Text.PrettyPrint.ANSI.Leijen
  ( Doc,
    comma,
    encloseSep,
    hardline,
    indent,
    lbracket,
    rbracket,
    semiBraces,
    text,
    tupled,
    (<$$>),
    (<$>),
    (<+>),
    (<//>),
    (</>),
    (<>),
  )
import Prelude hiding ((<$>))

source :: ProofTree f v -> Source f v
source Empty = Initial
source (Cons p _) = source p
source (Collect _ s _) = s
source (Proof s _) = s
source (Source s) = s

proofs :: ProofTree f v -> [(Source f v, Method f v)]
proofs Empty = []
proofs (Cons x y) = proofs x ++ proofs y
proofs (Collect _ _ xs) = concatMap proofs xs
proofs (Proof s m) = [(s, m)]
proofs (Source s) = []

rename :: (Ord v, Ord v') => (v -> v') -> ProofTree f v -> ProofTree f v'
rename v Empty = Empty
rename v (Cons p1 p2) = Cons (rename v p1) (rename v p2)
rename v (Collect p s ps) =
  Collect (Problem.rename v p) (renameSource v s) (map (rename v) ps)
rename v (Proof s m) = Proof (renameSource v s) (renameMethod v m)
rename v (Source s) = Source (renameSource v s)

renameMethod :: (Ord v, Ord v') => (v -> v') -> Method f v -> Method f v'
renameMethod v (Tcap p) = Tcap (Problem.rename v p)
renameMethod v (Stg p) = Stg (Problem.rename v p)
renameMethod v (Unif p) = Unif (Problem.rename v p)
renameMethod v (MergeSubst s t) =
  MergeSubst (renameSubst v s) (renameSubst v t)

renameSource :: (v -> v') -> Source f v -> Source f v'
renameSource v (Transitivity p1 p2) =
  Transitivity (Problem.rename v p1) (Problem.rename v p2)
renameSource v (SameLhs p1 p2) =
  SameLhs (Problem.rename v p1) (Problem.rename v p2)
renameSource v (SameRhs p1 p2) =
  SameRhs (Problem.rename v p1) (Problem.rename v p2)
renameSource _ Initial = Initial
renameSource _ ChangeMe = ChangeMe
renameSource _ LNarrowed = LNarrowed
renameSource _ RootDecomp = RootDecomp

prettySource :: (f -> Doc) -> (v -> Doc) -> Source f v -> Doc
prettySource f v Initial = text "input"
prettySource f v ChangeMe = text "undefined"
prettySource f v LNarrowed = text "narrowing"
prettySource f v RootDecomp = text "root-nonreachability"
prettySource f v (Transitivity p1 p2) =
  text "transitivity between"
    </> prettyProblem f v p1
    </> text "and"
    </> prettyProblem f v p2
prettySource f v (SameLhs p1 p2) =
  text "equal lhs of"
    </> prettyProblem f v p1
    </> text "and"
    </> prettyProblem f v p2
prettySource f v (SameRhs p1 p2) =
  text "equal rhs of"
    </> prettyProblem f v p1
    </> text "and"
    </> prettyProblem f v p2

prettyMethod :: (f -> Doc) -> (v -> Doc) -> Method f v -> Doc
prettyMethod f v (Tcap p) = text "using TCAP on" <$$> prettyProblem f v p
prettyMethod f v (Stg p) =
  text "using the symbol transition graph on" <$$> prettyProblem f v p
prettyMethod f v (Unif p) =
  text "since unification failed for" <$$> prettyProblem f v p
prettyMethod f v (MergeSubst s1 s2) =
  text "since required substitutions could not be merged"
    </> prettySubst f v s1
    </> text "and"
    </> prettySubst f v s2

prettyProof :: (f -> Doc) -> (v -> Doc) -> ProofTree f v -> Doc -> Doc
prettyProof fun var pt prefix =
  prefix
    <$> hardline
      <> go pt
    <$> hardline
      <> text
        "where | is used as a fresh function symbol, since it is illegal in the input format."
  where
    go Empty = text "Empty Proof"
    go (Proof s m) =
      text "problem obtained from"
        </> prettySource fun var s
        </> text "is proven infeasible"
        </> prettyMethod fun var m
    go (Source s) = text "because of" <+> prettySource fun var s
    go (Cons p1 p2) = go p1 <$$> text "CONS" <$$> go p2
    go (Collect p s pts) =
      text "problem"
        <+> prettyProblem fun var p
        <$$> text "obtained from"
          </> prettySource fun var s
        <$$> text "requires"
        <$$> indent 2 (encloseSep lbracket rbracket comma (map go pts))
