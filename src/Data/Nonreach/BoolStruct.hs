module Data.Nonreach.BoolStruct where

data BoolStruct t u v = Bottom u | Top u | Any v [BoolStruct t u v] | All v [BoolStruct t u v] | Atom t u deriving (Show, Eq)

rename :: (a -> b) -> BoolStruct a u v -> BoolStruct b u v
rename f (Any v xs) = Any v $ map (rename f) xs
rename f (All v xs) = All v $ map (rename f) xs
rename f (Atom x y) = Atom (f x) y
rename _ (Bottom y) = Bottom y
rename _ (Top y) = Top y

map' ::
  (a -> b) ->
  (u -> u') ->
  (v -> v') ->
  BoolStruct a u v ->
  BoolStruct b u' v'
map' f g h (Any v xs) = Any (h v) $ map (map' f g h) xs
map' f g h (All v xs) = All (h v) $ map (map' f g h) xs
map' f g _ (Atom x y) = Atom (f x) (g y)
map' _ g _ (Bottom y) = Bottom (g y)
map' _ g _ (Top y) = Top (g y)

-- NOTE unused and outdated
-- flatten :: BoolStruct t u -> [t]
-- flatten (Any _   ) = undefined -- FIXME this is not good
-- flatten (All xs  ) = concatMap flatten xs
-- flatten (Atom x _) = [x]
-- flatten (Bottom _) = undefined
-- flatten (Top    _) = undefined
--
--
-- -- FIXME why not pretty print?
-- -- either replace or remove this
-- prettyBS :: (Show t, Show u) => BoolStruct t u -> String
-- prettyBS = go ""
--  where
--   tab = "  "
--   go p (Any xs) =
--     p
--       ++ "ANY ["
--       ++ concatMap (\x -> "\n" ++ p ++ go (tab ++ p) x ++ " OR ") xs
--       ++ "\n"
--       ++ p
--       ++ "]"
--   go p (All xs) =
--     p
--       ++ "ALL ["
--       ++ concatMap (\x -> "\n" ++ p ++ go (tab ++ p) x ++ " AND ") xs
--       ++ "\n"
--       ++ p
--       ++ "]"
--   go p (Atom x s) = p ++ "Atom {" ++ show x ++ "} (" ++ show s ++ ")"
--   go p (Bottom s) = p ++ "Bottom (" ++ show s ++ ")"
--   go p (Top    s) = p ++ "Top (" ++ show s ++ ")"
