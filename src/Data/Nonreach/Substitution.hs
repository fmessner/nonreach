module Data.Nonreach.Substitution
  ( Substitution,
    emptySubst,
    emptySubst',
    renameSubst,
    isValid,
    isMinimal,
    nonMinimal,
    fromSubst,
    toSubst,
    merge,
    merge',
    mergeSubst,
    compose,
  )
where

import qualified Data.Map.Strict as M
import Data.Maybe
  ( fromJust,
    isJust,
  )
import Data.Rewriting.Substitution (Subst)
import qualified Data.Rewriting.Substitution as Subst
import qualified Data.Rewriting.Substitution.Type as SubstT
import qualified Data.Rewriting.Term as T

-- Just (Subst, isMinimal)
-- Nothing -> MAYBE
type Substitution f v = Maybe (Subst.Subst f v, Bool)

isMinimal :: Substitution f v -> Bool
isMinimal Nothing = False
isMinimal (Just s) = snd s

nonMinimal :: Substitution f v -> Substitution f v
nonMinimal Nothing = Nothing
nonMinimal (Just (s, _)) = Just (s, False)

isValid :: Substitution f v -> Bool
isValid = isJust

fromSubst :: Subst f v -> Substitution f v
fromSubst s = Just (s, True)

toSubst :: Substitution f v -> Subst f v
toSubst = fst . fromJust

emptySubst :: Substitution f v
emptySubst = Just (emptySubst', True)

emptySubst' :: Subst f v
emptySubst' = SubstT.fromMap M.empty

renameSubst :: (Ord v, Ord v') => (v -> v') -> Subst f v -> Subst f v'
renameSubst v s =
  SubstT.fromMap . M.map (T.rename v) . M.mapKeys v $ SubstT.toMap s

merge ::
  (Eq f, Ord v) =>
  Substitution f v ->
  Substitution f v ->
  Maybe (Substitution f v)
merge Nothing _ = Just Nothing
merge _ Nothing = Just Nothing
merge (Just (s, bs)) (Just (t, bt)) = case Subst.merge s t of
  Nothing -> Nothing
  Just s' -> Just $ Just (s', bs && bt)

merge' ::
  (Eq f, Ord v) => Substitution f v -> Subst f v -> Maybe (Substitution f v)
merge' s t = merge s (fromSubst t)

mergeSubst ::
  (Eq f, Ord v) => Subst f v -> Subst f v -> Maybe (Substitution f v)
mergeSubst s t = merge (fromSubst s) (fromSubst t)

compose :: (Ord v) => Substitution f v -> Substitution f v -> Substitution f v
compose Nothing _ = Nothing
compose _ Nothing = Nothing
compose (Just (s, bs)) (Just (t, bt)) = Just (Subst.compose s t, bs && bt)
