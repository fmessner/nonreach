module Data.Nonreach.Problem.Type
  ( Problem (..),
    isReachability,
  )
where

import Data.Rewriting.Substitution (Subst)
import Data.Rewriting.Term (Term (..))

data Problem f v
  = Reachability {src :: Term f v, tgt :: Term f v}
  | -- the following don't really have a 'source' and 'target' but I prefer
    -- consistent naming here for convenience later on
    Unification {src :: Term f v, tgt :: Term f v}
  | Meetability {src :: Term f v, tgt :: Term f v, root :: Maybe (Term f v)}
  | Joinability {src :: Term f v, tgt :: Term f v, end :: Maybe (Term f v)}
  deriving (Ord, Eq, Show)

isReachability :: Problem f v -> Bool
isReachability (Reachability _ _) = True
isReachability _ = False
