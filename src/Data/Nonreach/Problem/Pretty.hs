module Data.Nonreach.Problem.Pretty where

import Data.Nonreach.Problem.Type (Problem (..))
import Data.Rewriting.Term
  ( Term (..),
    prettyTerm,
  )
import Text.PrettyPrint.ANSI.Leijen
  ( Doc,
    hardline,
    semiBraces,
    text,
    tupled,
    (<$>),
    (<+>),
    (</>),
    (<>),
  )

prettyProblem :: (f -> Doc) -> (v -> Doc) -> Problem f v -> Doc
prettyProblem f v (Reachability s t) =
  prettyTerm f v s <+> text "->*" <+> prettyTerm f v t
prettyProblem f v (Joinability s t Nothing) =
  prettyTerm f v s <+> text "->* . *<-" <+> prettyTerm f v t
prettyProblem f v (Joinability s t (Just u)) =
  prettyTerm f v s
    <+> text "->*"
    <+> prettyTerm f v u
    <+> text "*<-"
    <+> prettyTerm f v t
prettyProblem f v (Meetability s t Nothing) =
  prettyTerm f v s <+> text "*<- . ->*" <+> prettyTerm f v t
prettyProblem f v (Meetability s t (Just u)) =
  prettyTerm f v s
    <+> text "*<-"
    <+> prettyTerm f v u
    <+> text "->*"
    <+> prettyTerm f v t
prettyProblem f v (Unification s t) =
  prettyTerm f v s <+> text "~" <+> prettyTerm f v t
