module Data.Nonreach.Problem.ProblemType
  ( ProblemType,
    ConnectiveMeta (..),
    emptyMeta,
    connectiveFromProblem,
    clearRenameCMeta,
    top,
    atom,
    atom',
    mapTermsPType,
    fromRule,
  )
where

import qualified Data.Map.Strict as M
import Data.Nonreach.BoolStruct (BoolStruct (..))
import qualified Data.Nonreach.BoolStruct as BS
import Data.Nonreach.Problem.Ops (mapTerms)
import Data.Nonreach.Problem.Type
import Data.Nonreach.ProofTree.Ops (source)
import Data.Nonreach.ProofTree.Type
import Data.Nonreach.Reachability.Type
  ( Result (..),
  )
import Data.Nonreach.Substitution
  ( Substitution,
    emptySubst,
    fromSubst,
  )
import Data.Rewriting.Rule (Rule (..))
import Data.Rewriting.Term (Term (..))

type ProblemMeta f v = (Substitution f v, ProofTree f v)

data ConnectiveMeta f v = ConnectiveMeta (Problem f v) (Source f v) [Result f v]

type ProblemType f v =
  BoolStruct (Problem f v) (ProblemMeta f v) (ConnectiveMeta f v)

emptyMeta :: ProblemMeta f v
emptyMeta = (emptySubst, Empty)
{-# INLINEABLE emptyMeta #-}

connectiveFromProblem :: Problem f v -> ProofTree f v -> ConnectiveMeta f v
connectiveFromProblem p pt = ConnectiveMeta p (source pt) []

-- This esentially removes meta data! Be careful!
clearRenameCMeta ::
  (Term f v -> Term f' v') -> ConnectiveMeta f v -> ConnectiveMeta f' v'
clearRenameCMeta f (ConnectiveMeta p _ _) =
  ConnectiveMeta (Reachability (f $ src p) (f $ tgt p)) Initial []

top :: ProofTree f v -> Substitution f v -> ProblemType f v
top pt s = Top (s, pt)
{-# INLINEABLE top #-}

atom :: Problem f v -> ProblemType f v
atom p = Atom p emptyMeta
{-# INLINEABLE atom #-}

atom' :: ProblemMeta f v -> Problem f v -> ProblemType f v
atom' s p = Atom p s
{-# INLINEABLE atom' #-}

fromRule :: Rule f v -> ProblemType f v
fromRule (Rule lhs rhs) = atom (Reachability lhs rhs)

-- NOTE This function drops the meta-data!
-- TODO make variation that renames it instead
mapTermsPType ::
  (Term f v -> Term f' v') -> ProblemType f v -> ProblemType f' v'
mapTermsPType f = BS.map' (mapTerms f) (const emptyMeta) (clearRenameCMeta f)
