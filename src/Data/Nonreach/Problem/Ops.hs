{-# LANGUAGE TupleSections #-}

module Data.Nonreach.Problem.Ops
  ( funs,
    vars,
    applySubst,
    rename,
    renameT,
    asTerm,
    renamerInitial,
    renamerDrop,
    mapTerms,
  )
where

import qualified Data.Nonreach.BoolStruct as BS
import Data.Nonreach.Problem.Type
import Data.Nonreach.ProofTree.Type (ProofTree (..))
import Data.Nonreach.Substitution (emptySubst)
import Data.Nonreach.Term
  ( VType,
    lift,
  )
import Data.Rewriting.Rule (Rule (..))
import Data.Rewriting.Substitution
  ( Subst,
    apply,
  )
import Data.Rewriting.Term (Term (..))
import qualified Data.Rewriting.Term as T

funs :: Problem f v -> [f]
funs p = T.funs (src p) ++ T.funs (tgt p)

vars :: Problem f v -> [v]
vars p = T.vars (src p) ++ T.vars (tgt p)

rename :: (v -> v') -> Problem f v -> Problem f v'
rename f (Reachability l r) = Reachability (T.rename f l) (T.rename f r)
rename f (Unification l r) = Unification (T.rename f l) (T.rename f r)
rename f (Meetability l r rt) =
  Meetability (T.rename f l) (T.rename f r) $ fmap (T.rename f) rt
rename f (Joinability l r e) =
  Joinability (T.rename f l) (T.rename f r) $ fmap (T.rename f) e

rename' :: (v -> v') -> (v -> v') -> Problem f v -> Problem f v'
rename' f f' (Reachability l r) = Reachability (T.rename f l) (T.rename f' r)
rename' f f' (Unification l r) = Unification (T.rename f l) (T.rename f' r)
-- rename' f f' (Meetability  l r rt) = Meetability (T.rename f l) (T.rename f' r) $ fmap (T.rename )rt

rename' _ _ Meetability {} = undefined -- FIXME what to do here? -- NOTE not a problem as of now, since only used for initial problems, which are always Reachability
rename' _ _ Joinability {} = undefined

renameT :: (Term f v -> Term f v') -> Problem f v -> Problem f v'
renameT f (Reachability u v) = Reachability (f u) (f v)
renameT f (Unification u v) = Unification (f u) (f v)
renameT f (Meetability u v r) = Meetability (f u) (f v) (fmap f r)
renameT f (Joinability u v e) = Joinability (f u) (f v) (fmap f e)

-- renameT f p                   = p { src = f $ src p, tgt = f $ tgt p }

asTerm :: Problem f v -> Term (Maybe f) v
asTerm p = lift (src p, tgt p)

applySubst :: (Ord v) => Subst f v -> Problem f v -> Problem f v
applySubst s c = c {src = apply s (src c), tgt = apply s (tgt c)}

renamerInitial :: String -> String -> Problem f v -> Problem f (VType v)
renamerInitial s t = rename' (0,[],s,) (0,[],t,)

-- handle with care!

renamerDrop :: Problem f (VType v) -> Problem f v
renamerDrop = rename' (\(_, _, _, x) -> x) (\(_, _, _, x) -> x)

mapTerms :: (Term f v -> Term f' v') -> Problem f v -> Problem f' v'
mapTerms f (Reachability u v) = Reachability (f u) (f v)
mapTerms f (Unification u v) = Unification (f u) (f v)
mapTerms f (Meetability u v r) = Meetability (f u) (f v) (fmap f r)
mapTerms f (Joinability u v e) = Joinability (f u) (f v) (fmap f e)
