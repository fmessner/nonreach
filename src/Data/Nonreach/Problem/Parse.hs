module Data.Nonreach.Problem.Parse
  ( fromString,
    fromString',
    fromFile,
  )
where

import Data.Nonreach.Problem.Type
import Data.Nonreach.Utils.Megaparse
import qualified Data.Set as Set
import Text.Megaparsec hiding (parse)
import Text.Megaparsec.Char
import Prelude hiding (lex)

type ParserState = Problem String String

type ProblemParser p = Parsec String String p

type PError = ParseErrorBundle String String

fromString' :: [String] -> String -> Either PError (Problem String String)
fromString' vs = runParser (problem vs) ""

fromString :: [String] -> String -> Either PError [Problem String String]
fromString vs = runParser (problems vs) ""

fromFile :: [String] -> String -> IO (Either PError [Problem String String])
-- fromFile vs = runP (problems vs) Problem {} ""
fromFile vs file = do
  fileContent <- readFile file
  return $ runParser (problems vs) "" fileContent

problem :: [String] -> ProblemParser ParserState
problem vs = do
  s <- parseWST varsSet
  _ <- lex $ string "->"
  t <- parseWST varsSet
  return Reachability {src = s, tgt = t}
  where
    varsSet = Set.fromList vs

problems :: [String] -> ProblemParser [ParserState]
problems = many . problem
