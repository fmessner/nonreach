-- This file is based on the problem parser from the 'term-rewriting' library.
{-# LANGUAGE FlexibleContexts #-}

module Data.Nonreach.ConditionalRewriting.Parse
  ( parseFileIO,
    parseIO,
  )
where

import Control.Exception (catch)
import Control.Monad (liftM)
import Control.Monad.Error
import Data.List
  ( partition,
    union,
  )
import Data.Maybe (fromMaybe)
import Data.Nonreach.ConditionalRewriting.Ops
import Data.Nonreach.ConditionalRewriting.Type as C
import Data.Nonreach.Problem (Problem (..))
import Data.Nonreach.Utils
  ( ident,
    lex,
    par,
  )
import qualified Data.Rewriting.Problem.Type as Prob hiding
  ( RulesPair (..),
  )
import qualified Data.Rewriting.Rule as R
import qualified Data.Rewriting.Rules as Rules
import qualified Data.Rewriting.Term as Term
import System.IO (readFile)
import Text.Parsec hiding (parse)
import Prelude hiding (lex)

data SystemParseError
  = UnknownParseError String
  | UnsupportedStrategy String
  | FileReadError IOError
  | UnsupportedDeclaration String
  | SomeParseError ParseError
  deriving (Show)

-- FIXME Error  is deprecated
instance Error SystemParseError where strMsg = UnknownParseError

type ParserState = CTRS String String

type WSTParser s a = ParsecT s ParserState (Either SystemParseError) a

parseFileIO :: FilePath -> IO ParserState
parseFileIO file = do
  r <- fromFile file
  case r of
    Left err -> do
      putStrLn "following error occured:"
      print err
      mzero
    Right x -> return x

parseIO :: String -> IO ParserState
parseIO str = case fromString str of
  Left err -> do
    putStrLn "following error occured:"
    print err
    mzero
  Right x -> return x

fromFile :: FilePath -> IO (Either SystemParseError ParserState)
fromFile file = fromFile' `catch` (return . Left . FileReadError)
  where
    fromFile' = fromCharStream sn `liftM` readFile file
    sn = "<file " ++ file ++ ">"

fromString :: String -> Either SystemParseError ParserState
fromString = fromCharStream "supplied string"

fromCharStream ::
  (Stream s (Either SystemParseError) Char) =>
  SourceName ->
  s ->
  Either SystemParseError ParserState
fromCharStream sourcename input =
  case runParserT parse initialState sourcename input of
    Right (Left e) -> Left $ SomeParseError e
    Right (Right p) -> Right p
    Left e -> Left e
  where
    initialState =
      ParseState
        { ctype = "ORIENTED",
          C.rules = RulesPair {strictRules = [], weakRules = []},
          variables = [],
          symbols = [],
          startTerms = Prob.AllTerms,
          strategy = Prob.Full,
          theory = Nothing,
          comment = Nothing,
          problems = Nothing
        }

parsedVariables :: WSTParser s [String]
parsedVariables = variables `liftM` getState

type CType = String

setConditionsType :: CType -> [Condition f v] -> [Condition f v]
setConditionsType ctype = map (go ctype)
  where
    go "ORIENTED" c = Reachability (src c) (tgt c)
    go "SEMI-EQUATIONAL" c = Reachability (src c) (tgt c)
    go "JOIN" c = Joinability (src c) (tgt c) Nothing

setRulesConditionType :: CType -> RulesPair f v -> RulesPair f v
setRulesConditionType ctype@"SEMI-EQUATIONAL" (RulesPair sr wr) =
  RulesPair
    (concatMap go sr)
    (concatMap go wr)
  where
    go (ConditionalRule l r cs) = [ConditionalRule r l (setConditionsType ctype cs), ConditionalRule l r (setConditionsType ctype cs)]
setRulesConditionType ctype (RulesPair sr wr) =
  RulesPair
    (map go sr)
    (map go wr)
  where
    go (ConditionalRule l r cs) = ConditionalRule l r (setConditionsType ctype cs)

parse :: (Stream s (Either SystemParseError) Char) => WSTParser s ParserState
parse = spaces >> parse' >> eof >> getState
  where
    parse' = parseProbs <|> parseDecls
    parseProbs = try $ do
      _ <- par $ do
        lex $ string "PROBLEM"
        lex $ string "INFEASIBILITY"
      many1 (parseDecl <|> parseProb)
    parseDecls = many1 parseDecl
    parseDecl =
      decl "CONDITIONTYPE" ctypeP (\e p -> p {ctype = e})
        <|> decl "VAR" varsP (\e p -> p {variables = e `union` variables p})
        <|> decl
          "RULES"
          rulesP
          ( \e p ->
              p
                { C.rules = setRulesConditionType (ctype p) e, -- FIXME multiple RULES blocks?
                  symbols = Rules.funsDL (assumeSatisfiedAll $ allRules e) []
                }
          )
        -- NOTE these results are unused
        <|> decl "THEORY" theoryP (\e p -> p {theory = maybeAppend theory e p})
        <|> decl "STRATEGY" strategyP (\e p -> p {strategy = e})
        <|> decl "STARTTERM" starttermsP (\e p -> p {startTerms = e})
        <|> decl
          "COMMENT"
          commentP
          (\e p -> p {comment = maybeAppend comment e p})
    -- TODO find out why this would be better
    -- <|> (try
    --       (   par commentP
    --       >>= modifyState
    --       .   (\e p -> p { comment = maybeAppend comment e p })
    --       <?> "comment"
    --       )
    -- )
    parseProb =
      decl
        "CONDITION"
        problemsP
        (\e p -> if null e then p else p {problems = Just $ setConditionsType (ctype p) e})
    decl name p f =
      try
        ( par $ do
            lex $ string name
            r <- p
            modifyState $ f r
        )
        <?> (name ++ " block")
    maybeAppend fld e p = Just $ fromMaybe [] (fld p) ++ e

ctypeP :: (Stream s (Either SystemParseError) Char) => WSTParser s String
ctypeP = lex $ try (string "ORIENTED") <|> try (string "SEMI-EQUATIONAL") <|> string "JOIN"

-- TODO add alternatives or at least throw meaningful error

varsP :: (Stream s (Either SystemParseError) Char) => WSTParser s [String]
varsP = many (lex $ ident "()," [])

rulesP ::
  (Stream s (Either SystemParseError) Char) =>
  WSTParser s (RulesPair String String)
rulesP = do
  vs <- parsedVariables
  rs <- many $ rule vs
  let (s, w) = partition fst rs
  return RulesPair {strictRules = map snd s, weakRules = map snd w}
  where
    rule vs = do
      l <- Term.parseWST vs
      sep <- lex $ try (string "->=") <|> string "->"
      r <- Term.parseWST vs
      cs <- conditionsP vs
      return (sep == "->", ConditionalRule {lhs = l, rhs = r, cond = cs})

problemsP ::
  (Stream s (Either SystemParseError) Char) =>
  WSTParser s [Condition String String]
problemsP = do
  vs <- parsedVariables
  conditionsP' vs

conditionsP ::
  (Stream s (Either SystemParseError) Char) =>
  [String] ->
  WSTParser s [Condition String String]
conditionsP vs = option [] $ try $ do
  lex $ string "|"
  conditionsP' vs

conditionsP' ::
  (Stream s (Either SystemParseError) Char) =>
  [String] ->
  WSTParser s [Condition String String]
conditionsP' vs = sepBy1 (conditionP vs) (lex $ string ",")

conditionP ::
  (Stream s (Either SystemParseError) Char) =>
  [String] ->
  WSTParser s (Condition String String)
conditionP vs = do
  s <- Term.parseWST vs
  _ <- lex (try (string "==") <|> string "=")
  t <- Term.parseWST vs
  return Reachability {src = s, tgt = t}

-- unused

theoryP ::
  (Stream s (Either SystemParseError) Char) =>
  WSTParser s [Prob.Theory String String]
theoryP = many thdecl
  where
    thdecl =
      par
        ( (Prob.Equations <$> equations)
            <|> (idlist >>= \(x : xs) -> return $ Prob.SymbolProperty x xs)
        )
    equations =
      try
        ( do
            vs <- parsedVariables
            lex $ string "EQUATIONS"
            many $ equation vs
        )
        <?> "EQUATIONS block"
    equation vs = do
      l <- Term.parseWST vs
      lex $ string "=="
      r <- Term.parseWST vs
      return $ R.Rule l r
    idlist = many1 $ lex (ident "()," [])

strategyP ::
  (Stream s (Either SystemParseError) Char) => WSTParser s Prob.Strategy
strategyP = innermost <|> outermost
  where
    innermost = string "INNERMOST" >> return Prob.Innermost
    outermost = string "OUTERMOST" >> return Prob.Outermost

starttermsP ::
  (Stream s (Either SystemParseError) Char) => WSTParser s Prob.StartTerms
starttermsP = basic <|> terms
  where
    basic = string "CONSTRUCTOR-BASED" >> return Prob.BasicTerms
    terms = string "FULL" >> return Prob.AllTerms

commentP :: (Stream s (Either SystemParseError) Char) => WSTParser s String
commentP = withpars <|> liftM2 (++) idents commentP <|> return ""
  where
    idents = many1 (noneOf "()")
    withpars = do
      _ <- char '('
      pre <- commentP
      _ <- char ')'
      suf <- commentP
      return $ "(" ++ pre ++ ")" ++ suf
