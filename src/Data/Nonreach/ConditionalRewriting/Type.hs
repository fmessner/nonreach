module Data.Nonreach.ConditionalRewriting.Type where

import Data.Nonreach.Problem (Problem (..))
import qualified Data.Rewriting.Problem as Prob
import Data.Rewriting.Term (Term (..))

-- FIXME make ConditionalRule dependent on type c; instantiate c with Problem
type Condition = Problem

data ConditionalRule f v = ConditionalRule {lhs :: Term f v, rhs :: Term f v, cond :: [Condition f v]}
  deriving (Ord, Eq, Show)

data RulesPair f v = RulesPair
  { strictRules :: [ConditionalRule f v],
    weakRules :: [ConditionalRule f v]
  }
  deriving (Eq, Show)

allRules :: RulesPair f v -> [ConditionalRule f v]
allRules rp = strictRules rp ++ weakRules rp

-- TODO ctype should not be a string
data CTRS f v = ParseState
  { ctype :: String,
    rules :: RulesPair f v,
    variables :: [v],
    symbols :: [f],
    theory :: Maybe [Prob.Theory f v],
    startTerms :: Prob.StartTerms,
    strategy :: Prob.Strategy,
    comment :: Maybe String,
    problems :: Maybe [Condition f v]
  }
  deriving (Show)

sRules :: CTRS f v -> [ConditionalRule f v]
sRules = strictRules . rules
