module Data.Nonreach.ConditionalRewriting.Ops
  ( funs,
    funsDup,
    vars,
    rename,
    renamerInitial,
    renamerDrop,
    assumeSatisfied,
    assumeSatisfiedAll,
    fromRule,
    fromRules,
    split,
    split',
    fullRewrite,
    inlineConditions,
    leftInlineConditions,
    filterOutCond,
    toProblem,
  )
where

import qualified Control.Arrow as CA
import Data.List (partition)
import qualified Data.Map as M
import Data.Maybe
  ( fromJust,
    isJust,
  )
import Data.Nonreach.BoolStruct (BoolStruct (..))
import Data.Nonreach.ConditionalRewriting.Type
import Data.Nonreach.Problem
  ( Problem (..),
    ProblemType,
    isReachability,
  )
import qualified Data.Nonreach.Problem as P
import Data.Nonreach.Substitution
  ( Substitution,
    fromSubst,
  )
import Data.Nonreach.Term (VType)
import qualified Data.Nonreach.Term as T'
import Data.Nonreach.Utils (nubOrd)
import Data.Rewriting.Pos (Pos)
import qualified Data.Rewriting.Rule as R
import Data.Rewriting.Substitution
  ( apply,
    unifyRef,
  )
import Data.Rewriting.Substitution.Type
  ( Subst (..),
    fromMap,
    toMap,
  )
import qualified Data.Rewriting.Term as T

-- functions lifted from Rule
funs :: Ord f => ConditionalRule f v -> [f]
funs (ConditionalRule l r cs) = nubOrd (T.funs l ++ T.funs r ++ fcs cs)
  where
    fcs = concatMap P.funs

-- potentially containing duplicates
funsDup :: Ord f => ConditionalRule f v -> [f]
funsDup (ConditionalRule l r cs) = T.funs l ++ T.funs r ++ fcs cs
  where
    fcs = concatMap P.funs

vars :: Ord v => ConditionalRule f v -> [v]
vars (ConditionalRule l r cs) = nubOrd (T.vars l ++ T.vars r ++ vcs cs)
  where
    vcs = concatMap P.vars

rename :: (v -> v') -> ConditionalRule f v -> ConditionalRule f v'
rename f (ConditionalRule l r cs) =
  ConditionalRule (T.rename f l) (T.rename f r) (map (P.rename f) cs)

renamerInitial :: String -> ConditionalRule f v -> ConditionalRule f (VType v)
renamerInitial s = rename (\x -> (0, [], s, x))

-- handle with care!
renamerDrop :: ConditionalRule f (VType v) -> ConditionalRule f v
renamerDrop = rename (\(_, _, _, x) -> x)

-- uniqueVars :: Ord v => a -> ConditionalRule f v -> ConditionalRule f (a, Int)
-- uniqueVars p r = rename (\x -> (p, m M.! x)) r
--  where
--   m  = M.fromList uv
--   uv = zip (nubOrd (vars r)) [1 ..]

--

-- Sternagel & Sternagel Draft 2019
-- Definition 22 (Inlining of Conditions) & Lemma 30
-- preserves ->*
inlineConditions ::
  (Ord v) =>
  [ConditionalRule f v] ->
  ( [ConditionalRule f v],
    [(ConditionalRule f v, (ConditionalRule f v, [Condition f v]))]
  )
inlineConditions rules =
  let rules' = map go rules
   in ( map fst rules',
        map (\(x, y) -> (x, fromJust y)) . filter (isJust . snd) $ rules'
      )
  where
    go c@(ConditionalRule _ _ []) = (c, Nothing)
    go rule@(ConditionalRule l r conds) =
      if all isReachability conds
        then f rule Nothing l r conds []
        else (rule, Nothing)
    f rl acc l r [] cs' = (ConditionalRule l r (reverse cs'), acc)
    f rl acc l r (c@(Reachability s (T.Var x)) : cs) cs' =
      if not
        ( elem
            x
            ( T.vars l
                ++ T.vars s
                ++ concatMap
                  (\(Reachability u v) -> T.vars v)
                  (cs ++ cs')
            )
        )
        then
          let sigma = fromMap $ M.fromList [(x, s)]
              subFirst (Reachability u v) = Reachability (apply sigma u) v
           in f
                rl
                ( case acc of
                    Nothing -> Just (rl, [c])
                    (Just (r', cs')) -> Just (r', c : cs')
                )
                l
                (apply sigma r)
                (map subFirst cs)
                (map subFirst cs')
        else f rl acc l r cs (c : cs')
    f rl acc l r (c : cs) cs' = f rl acc l r cs (c : cs')

leftInlineConditions ::
  (Ord v) =>
  [ConditionalRule f v] ->
  ( [ConditionalRule f v],
    [(ConditionalRule f v, (ConditionalRule f v, [Condition f v]))]
  )
leftInlineConditions rules =
  let rules' = map go rules
   in ( map fst rules',
        map (\(x, y) -> (x, fromJust y)) . filter (isJust . snd) $ rules'
      )
  where
    go c@(ConditionalRule _ _ []) = (c, Nothing)
    go rule@(ConditionalRule l r conds) =
      if all isReachability conds
        then f rule Nothing l r conds []
        else (rule, Nothing)
    f rl acc l r [] cs' = (ConditionalRule l r (reverse cs'), acc)
    f rl acc l r (c@(Reachability (T.Var x) t) : cs) cs' =
      if not
        ( elem
            x
            ( T.vars r
                ++ T.vars t
                ++ concatMap
                  (\(Reachability u v) -> T.vars u)
                  (cs ++ cs')
            )
        )
        then
          let sigma = fromMap $ M.fromList [(x, t)]
           in f
                rl
                ( case acc of
                    Nothing -> Just (rl, [c])
                    (Just (r', cs')) -> Just (r', c : cs')
                )
                (apply sigma l)
                r
                cs
                cs'
        else f rl acc l r cs (c : cs')
    f rl acc l r (c : cs) cs' = f rl acc l r cs (c : cs')

filterOutCond ::
  (Ord v, Ord f) =>
  [Problem f v] ->
  [ConditionalRule f v] ->
  ([ConditionalRule f v], [ConditionalRule f v])
filterOutCond conds rules =
  let (dropped, kept) = partition go rules in (kept, dropped)
  where
    toFun xs =
      T.Fun
        Nothing
        ( map (\x -> T.Fun Nothing [T.map Just id (src x), T.map Just id (tgt x)]) xs
        )
    conds' = toFun conds
    go (ConditionalRule l r cs) = T'.equalModuloRenaming conds' (toFun cs)

--

assumeSatisfied :: ConditionalRule f v -> R.Rule f v
assumeSatisfied (ConditionalRule l r _) = R.Rule l r

assumeSatisfiedAll :: [ConditionalRule f v] -> [R.Rule f v]
assumeSatisfiedAll = map assumeSatisfied

fromRule :: R.Rule f v -> ConditionalRule f v
fromRule (R.Rule l r) = ConditionalRule l r []

fromRules :: [R.Rule f v] -> [ConditionalRule f v]
fromRules = map fromRule

-- NOTE unused
-- asRule
--   :: (R.Rule f v -> R.Rule f v) -> ConditionalRule f v -> ConditionalRule f v
-- asRule f c = ConditionalRule (R.lhs r) (R.rhs r) (cond c)
--   where r = assumeSatisfied c

-- split list of potentially conditional rules into lists of unconditional rules
-- and conditional ones
split :: [ConditionalRule f v] -> ([R.Rule f v], [ConditionalRule f v])
split = CA.first assumeSatisfiedAll . split'

split' ::
  [ConditionalRule f v] -> ([ConditionalRule f v], [ConditionalRule f v])
split' = partition (null . cond)
{-# INLINEABLE split' #-}

subterms' :: T.Term f v -> [(Pos, T.Term f v)]
subterms' = subt []
  where
    subt :: Pos -> T.Term f v -> [(Pos, T.Term f v)]
    subt i t = (i, t) : properSubt i t
    properSubt :: Pos -> T.Term f v -> [(Pos, T.Term f v)]
    properSubt _ (T.Var _) = []
    properSubt i (T.Fun _ ts) =
      map
        (prepend i)
        (concatMap (\(index, term) -> subt [index] term) $ zip [0 ..] ts)
    prepend :: Pos -> (Pos, T.Term f v) -> (Pos, T.Term f v)
    prepend i (pos, term) = (i ++ pos, term)

-- assuming variables in t are disjoint from vars in rules
fullRewrite ::
  (Ord v, Ord f) =>
  [ConditionalRule f (VType v)] ->
  (Int -> T.Term f (VType v) -> T.Term f (VType v)) ->
  T.Term f (VType v) ->
  T.Term f (VType v) ->
  [ ( T.Term f (VType v),
      T.Term f (VType v),
      [Condition f (VType v)],
      Substitution f (VType v)
    )
  ]
fullRewrite rs rn t t' =
  map
    ( \((p, s, r, cs), i) -> do
        let subst = fromJust s
        let u = apply subst . fromJust $ T.replaceAt t p r
        -- for nonreachability checks (vars of t and t' are disjoint) v == t'
        let v = apply subst t'
        let conds = map (P.applySubst subst) cs
        let subst' =
              fromMap
                . M.fromAscList
                . map (\(k, val) -> (k, rn i val))
                . filter (\(k, _) -> k `elem` T.vars t)
                . M.toAscList
                $ toMap subst
        (rn i u, rn i v, map (P.renameT (rn i)) conds, fromSubst subst')
        -- FIXME verify this is correct
    )
    $ zip
      ( filter (\(_, s, _, _) -> isJust s) $
          concatMap
            ( \r ->
                map
                  (\(p, s) -> (p, unifyRef (lhs r) s, rhs r, cond r))
                  (dropVars $ subterms' t)
            )
            rs
      )
      [0 ..]
  where
    disjoint = T'.isVarDisjoint t t'
    -- drop subterms that are variables as long as they only occur once
    -- because in that case the rewrite is covered by substitutions
    -- TODO but outside we already drop non-linears...
    dropVars
      | disjoint = filter f
      | otherwise = id
      where
        f (_, T.Var v) = cntOcc v (T.vars t) /= 1
        f (_, T.Fun _ _) = True
    cntOcc :: Eq t => t -> [t] -> Int
    cntOcc = go 0
      where
        go i _ [] = i
        go i x (y : ys) = go (if x == y then i + 1 else i) x ys

toProblem :: ConditionalRule f v -> P.ProblemType f v
toProblem (ConditionalRule lhs rhs _) = P.atom (P.Reachability lhs rhs)
