module Data.Nonreach.Reachability.Type
  ( Result (..),
    isYes,
    isNo,
    fromSubstitution,
  )
where

import Data.Nonreach.ProofTree.Type (ProofTree)
import Data.Nonreach.Substitution
  ( Substitution,
    isValid,
  )

data Result f v = YES (Substitution f v) | NO (ProofTree f v) | MAYBE deriving (Show)

fromSubstitution :: Substitution f v -> Result f v
fromSubstitution s = if isValid s then YES s else MAYBE

instance Eq (Result f v) where
  (==) (YES _) (YES _) = True
  (==) (NO _) (NO _) = True
  (==) MAYBE MAYBE = True
  (==) _ _ = False

isYes :: Result f v -> Bool
isYes (YES _) = True
isYes _ = False

isNo :: Result f v -> Bool
isNo (NO _) = True
isNo _ = False
