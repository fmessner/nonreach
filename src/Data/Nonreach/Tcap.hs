module Data.Nonreach.Tcap
  ( reach,
    rootReachable,
  )
where

import qualified Data.Nonreach.ConditionalRewriting as CR
import Data.Nonreach.Problem (Problem (..))
import Data.Nonreach.ProofTree
  ( Method (..),
    ProofTree (..),
    Source (..),
  )
import Data.Nonreach.Reachability (Result (..))
import Data.Nonreach.Utils (foldr2)
import Data.Rewriting.Rule (Rule (..))
import qualified Data.Rewriting.Rule as R
import Data.Rewriting.Rules (lhss)
import Data.Rewriting.Substitution
  ( apply,
    unifyRef,
  )
import Data.Rewriting.Term
  ( Term (..),
    rename,
  )

-- ground matching
-- rewritten from OCaml implementation in TTT2
simplify :: Eq f => [(Term f v, Term f v)] -> Maybe [(Term f v, Term f v)]
simplify [] = Just []
simplify ((Var _, _) : ps) = simplify ps
simplify ((Fun f us, Fun g vs) : ps) =
  if f == g then simplify (zip us vs ++ ps) else Nothing
simplify ((t, Var x) : ps) = fmap (\xs -> (t, Var x) : xs) (simplify ps)

merge :: Eq f => Eq v => Term f v -> Term f v -> Maybe (Term f v)
merge (Var _) u = Just u
merge u (Var _) = Just u
merge (Fun g us) (Fun h vs) =
  if g == h
    then
      fmap
        (Fun g)
        ( foldr2
            (\(u, v) -> maybe Nothing (\ws -> fmap (: ws) (merge u v)))
            us
            vs
            (Just [])
        )
    else Nothing

extend ::
  Eq f =>
  Eq v =>
  Term f v ->
  [(Term f v, Term f v)] ->
  Term f v ->
  Maybe [(Term f v, Term f v)]
extend _ [] _ = Just []
extend x ((v, y) : ps) u =
  if x == y
    then maybe Nothing (extend x ps) (merge u v)
    else fmap (\zs -> (v, y) : zs) (extend x ps u)

solve :: Eq f => Eq v => [(Term f v, Term f v)] -> Bool
solve [] = True
solve ((u, x) : ps) = maybe False solve (extend x ps u)

groundMatches :: Eq f => Eq v => Term f v -> Term f v -> Bool
groundMatches u v = groundMatchesList [(u, v)]
  where
    groundMatchesList = maybe False solve . simplify

-- Reachability checks
reach ::
  (Ord f, Ord v) =>
  [CR.ConditionalRule f v] ->
  Source f v ->
  Problem f v ->
  Result f v
reach _ _ (Reachability (Var _) _) = MAYBE
reach _ _ (Reachability _ (Var _)) = MAYBE
reach crs src p@(Reachability s t) =
  if groundMatches cap (rename Just t)
    then MAYBE
    else NO (Proof src $ Tcap p)
  where
    cap = tcap (CR.assumeSatisfiedAll crs) s
-- where cap = ctcap crs s
reach _ _ (Joinability (Var _) _ _) = MAYBE
reach _ _ (Joinability _ (Var _) _) = MAYBE
reach crs src p@(Joinability u v _) =
  if groundMatches (tcap rs u) (tcap rs v)
    then MAYBE
    else NO (Proof src $ Tcap p)
  where
    rs = CR.assumeSatisfiedAll crs
-- TODO potentially find Meetability check with tcap
reach _ _ (Meetability (Var _) _ _) = MAYBE
reach _ _ (Meetability _ (Var _) _) = MAYBE
reach crs src p@(Meetability u v _) =
  if any R.isCollapsing rs' || groundMatches (tcap rs u) (tcap rs v)
    then MAYBE
    else NO (Proof src $ Tcap p)
  where
    rs' = CR.assumeSatisfiedAll crs
    rs = map (flipRule) rs'
    flipRule (Rule l r) = Rule r l
reach _ _ _ = MAYBE

rootReachable ::
  (Eq f, Eq v) => [CR.ConditionalRule f v] -> Term f v -> Term f v -> Bool
rootReachable rs s _ = tcap (CR.assumeSatisfiedAll rs) s == Var Nothing

-- TODO don't just ignore conditions of rules

tcap :: (Eq f, Eq v) => [Rule f v] -> Term f v -> Term f (Maybe v)
tcap rs s = tcap' (rename Just s)
  where
    fresh = Var Nothing
    tcap' (Var _) = fresh
    tcap' (Fun f args) = if any (groundMatches t') lhss' then fresh else t'
      where
        t' = Fun f (map tcap' args)
        lhss' = map (rename Just) (lhss rs)

-- NOTE no new results due to this
ctcap ::
  (Ord f, Ord v) =>
  [CR.ConditionalRule f v] ->
  Term f v ->
  Term f (Either v [Int])
ctcap crs s = tcap' (rename Left s) []
  where
    (rs, cs) = CR.split crs
    tcap' (Var _) pos = Var $ Right pos
    tcap' (Fun f args) pos =
      if any (groundMatches t') lhss'
        then Var $ Right pos
        else go cs
      where
        go [] = t'
        go (CR.ConditionalRule l _ conds : rs') =
          if y
            then Var $ Right pos
            else go rs'
          where
            y =
              maybe False (\sub -> all (checkCond sub) conds) $
                unifyRef (rename Left $ l) t'
            checkCond sub (Reachability u v) =
              groundMatches
                cap
                (rename Just $ apply sub (rename Left $ v))
              where
                cap =
                  tcap (map (R.rename Left) $ CR.assumeSatisfiedAll crs) $
                    apply sub (rename Left $ u)
            checkCond sub _ = True
        t' =
          Fun f (map (uncurry tcap') (zipWith (\x i -> (x, i : pos)) args [1 ..]))
        lhss' = map (rename Left) (lhss rs)
