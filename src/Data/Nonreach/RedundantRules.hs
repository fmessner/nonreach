module Data.Nonreach.RedundantRules
  ( redundantRules,
  )
where

import Data.List (partition)
import qualified Data.Nonreach.ConditionalRewriting as C
import Data.Nonreach.Problem (Problem (..))
import qualified Data.Nonreach.Problem as Problem
import Data.Nonreach.Reachability
  ( Result (..),
    isYes,
  )
import Data.Rewriting.Term (Term (..))

varsAsConst :: Term f v -> Term (Either f v) v
varsAsConst (Var x) = Fun (Right x) []
varsAsConst (Fun f xs) = Fun (Left f) (map varsAsConst xs)

undoVarsAsConst :: Term (Either f v) v -> Term f v
undoVarsAsConst (Var x) = Var x
undoVarsAsConst (Fun (Right x) _) = Var x
undoVarsAsConst (Fun (Left x) xs) = Fun x (map undoVarsAsConst xs)

varsAsConstP :: Problem f v -> Problem (Either f v) v
varsAsConstP = Problem.mapTerms varsAsConst

undoVarsAsConstP :: Problem (Either f v) v -> Problem f v
undoVarsAsConstP = Problem.mapTerms undoVarsAsConst

varsAsConstPType ::
  Problem.ProblemType f v -> Problem.ProblemType (Either f v) v
varsAsConstPType = Problem.mapTermsPType varsAsConst

varsAsConstCR :: C.ConditionalRule f v -> C.ConditionalRule (Either f v) v
varsAsConstCR (C.ConditionalRule l r cs) =
  C.ConditionalRule (varsAsConst l) (varsAsConst r) (map varsAsConstP cs)

undoVarsAsConstCR :: C.ConditionalRule (Either f v) v -> C.ConditionalRule f v
undoVarsAsConstCR (C.ConditionalRule l r cs) =
  C.ConditionalRule
    (undoVarsAsConst l)
    (undoVarsAsConst r)
    (map undoVarsAsConstP cs)

funsLeftCR :: C.ConditionalRule f v -> C.ConditionalRule (Either f v) v
funsLeftCR (C.ConditionalRule l r cs) =
  C.ConditionalRule
    (funsLeft l)
    (funsLeft r)
    (map funsLeftP cs)
  where
    funsLeft (Fun f xs) = Fun (Left f) (map funsLeft xs)
    funsLeft (Var x) = Var x
    funsLeftP = Problem.mapTerms funsLeft

undoFunsLeftCR :: C.ConditionalRule (Either f v) v -> C.ConditionalRule f v
undoFunsLeftCR (C.ConditionalRule l r cs) =
  C.ConditionalRule
    (undoFunsLeft l)
    (undoFunsLeft r)
    (map undoFunsLeftP cs)
  where
    undoFunsLeft (Fun (Left f) xs) = Fun f (map undoFunsLeft xs)
    undoFunsLeft (Fun (Right f) _) = Var f
    undoFunsLeft (Var x) = Var x
    undoFunsLeftP = Problem.mapTerms undoFunsLeft

-- TODO move out if it works
-- redundantRules
--   :: (Ord f, Ord v)
--   => ([C.ConditionalRule f v] -> ProblemType f v -> Result f v)
--   -> [C.ConditionalRule f v]
--   -> [C.ConditionalRule f v]
redundantRules _ [] = ([], [])
redundantRules reach cs' = go rs' [] []
  where
    (rs', cs) = C.split' cs'
    (removedCondrules, condRules) =
      partition
        (isYes . reach (map funsLeftCR rs') . varsAsConstPType . C.toProblem)
        cs
    go [] keep remove = (keep ++ condRules, remove ++ removedCondrules)
    go (r : rs) keep remove =
      case reach (map funsLeftCR $ keep ++ rs) (varsAsConstPType $ C.toProblem r) of
        YES _ -> go rs keep (r : remove)
        _ -> go rs (r : keep) remove
