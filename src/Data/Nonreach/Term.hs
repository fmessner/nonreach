{-# LANGUAGE TupleSections #-}

module Data.Nonreach.Term
  ( VType,
    renamerInitial,
    renamerNarrow,
    renamerBack,
    renameVType,
    isUnifiable,
    equalModuloRenaming,
    countVar,
    lift,
    isVarDisjoint,
  )
where

import Data.List (nub)
import Data.List.Extra (disjoint)
import qualified Data.Map.Strict as M
import Data.Maybe (isJust)
import Data.Rewriting.Substitution (unifyRef)
import Data.Rewriting.Term
  ( Term (..),
    rename,
    vars,
  )

type VType v = (Int, [Int], String, v)

renamerInitial :: String -> Term f v -> Term f (VType v)
renamerInitial s = rename (0,[],s,)

renamerNarrow :: Int -> Int -> Term f (VType v) -> Term f (VType v)
renamerNarrow n i = rename go
  where
    go (0, is, "r", x) = (n, i : is, "r", x)
    go v = v

renamerBack :: Term f (VType v) -> Term f (Maybe v)
renamerBack =
  rename
    ( \(_, _, s, x) -> case s of
        "s" -> Just x
        "t" -> Just x
        _ -> Nothing
    )

renameVType :: v -> VType v -> v
renameVType v (_, _, s, x) = case s of
  "s" -> x
  "t" -> x
  _ -> v

isUnifiable :: (Ord v, Ord f) => Term f v -> Term f v -> Bool
isUnifiable s t = isJust $ unifyRef s t

equalModuloRenaming :: (Ord v, Ord v', Eq f) => Term f v -> Term f v' -> Bool
equalModuloRenaming s t = s' == t'
  where
    s' = rename (\x -> ms M.! x) s
    t' = rename (\x -> mr M.! x) t
    ms = M.fromList $ zip (nub $ vars s) [1 ..]
    mr = M.fromList $ zip (nub $ vars t) [1 ..]

countVar :: (Eq v) => v -> Term f v -> Int
countVar x (Var y) = if x == y then 1 else 0
countVar x (Fun _ us) = sum $ map (countVar x) us

lift :: (Term f v, Term f v) -> Term (Maybe f) v
lift (u, v) = Fun Nothing [go u, go v]
  where
    go :: Term f v -> Term (Maybe f) v
    go (Fun f xs) = Fun (Just f) (map go xs)
    go (Var x) = Var x

isVarDisjoint u v = disjoint (vars u) (vars v)
