{-# LANGUAGE FlexibleContexts #-}

module Data.Nonreach.Utils.Megaparse
  ( Parser,
    lex,
    parseWST,
  )
where

import Control.Monad (guard)
import Control.Monad.Identity
import Data.Char (isSpace)
import Data.Rewriting.Term (Term (..))
import Data.Set
  ( Set,
    member,
    notMember,
  )
import qualified Data.Set as Set
import Text.Megaparsec hiding (parse)
import Text.Megaparsec.Char
import Prelude hiding (lex)

type Parser s = Parsec String String s

-- the following functions are based on code from
-- Data.Rewriting.Utils.Parse and Data.Rewriting.Term.Parse

lex :: Parser s -> Parser s
lex p = p <* space
{-# INLINE lex #-}

par :: Parser s -> Parser s
par = between (lex $ char '(') (lex $ char ')')
{-# INLINE par #-}

ident :: Set Char -> Parser String
ident tabooChars =
  some (satisfy (\c -> not (isSpace c) && c `notMember` tabooChars))
{-# INLINE ident #-}

parseFun :: Parser String -> Parser String
parseFun i = lex i <?> "function symbol"
{-# INLINE parseFun #-}

parseVar :: Parser String -> Set String -> Parser String
parseVar i xs =
  do
    x <- lex i
    guard (x `member` xs)
    return x
    <?> "variable"
{-# INLINE parseVar #-}

identWST :: Parser String
identWST = ident (Set.fromList "(),")
{-# INLINE identWST #-}

parse :: Parser String -> Parser String -> Parser (Term String String)
parse fun var = term <?> "term"
  where
    term = try (fmap Var var) <|> liftM2 Fun fun args
    args = par (sepBy term (lex $ char ',')) <|> return []
{-# INLINE parse #-}

parseWST :: Set String -> Parser (Term String String)
parseWST xs = parse (parseFun identWST) (parseVar identWST xs)
{-# INLINE parseWST #-}
