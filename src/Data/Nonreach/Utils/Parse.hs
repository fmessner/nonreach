{-# LANGUAGE FlexibleContexts #-}

module Data.Nonreach.Utils.Parse
  ( lex,
    par,
    ident,
  )
where

import Control.Monad (guard)
import Data.Char (isSpace)
import Text.Parsec hiding (parse)
import Prelude hiding (lex)

-- The following functions were taken from the 'term-rewriting' library, since
-- they are hidden. (lex, par)

-- | @lex p@ is similar to @p@ but also consumes trailing white space.
lex :: Stream s m Char => ParsecT s u m a -> ParsecT s u m a
lex p = do
  x <- p
  spaces
  return x
{-# INLINEABLE lex #-}

-- | @par p@ accpets @p@ enclosed in parentheses ('@(@' and '@)@').
par :: Stream s m Char => ParsecT s u m a -> ParsecT s u m a
par = between (lex $ char '(') (lex $ char ')')
{-# INLINEABLE par #-}

-- | @ident taboo@ parses a non-empty sequence of non-space characters not
-- containing elements of @taboo@.
ident :: Stream s m Char => String -> [String] -> ParsecT s u m String
ident tabooChars tabooWords = try $ do
  s <- many1 (satisfy (\c -> not (isSpace c) && c `notElem` tabooChars))
  guard (s `notElem` tabooWords)
  return s
{-# INLINEABLE ident #-}
