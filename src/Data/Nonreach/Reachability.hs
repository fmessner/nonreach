module Data.Nonreach.Reachability
  ( module Data.Nonreach.Reachability.Type,
    simplify,
    isReachable,
    exploit,
    exploit',
    stripAtoms,
  )
where

-- ( BoolStruct(..) )

import Data.List (partition)
import Data.Maybe (mapMaybe)
import Data.Nonreach.BoolStruct
import Data.Nonreach.Problem
  ( ConnectiveMeta (..),
    Problem (..),
    ProblemType,
    top,
  )
import Data.Nonreach.ProofTree
  ( Method (..),
    ProofTree (..),
    Source (..),
    source,
  )
import Data.Nonreach.Reachability.Type
import Data.Nonreach.Substitution
  ( Substitution,
    compose,
    emptySubst,
    emptySubst',
    fromSubst,
    isMinimal,
    isValid,
    merge,
    nonMinimal,
    toSubst,
  )
import Data.Nonreach.Term (isUnifiable)
import Data.Nonreach.Utils
  ( cartProd,
    cartProd',
  )
import Data.Rewriting.Substitution
  ( Subst,
    unifyRef,
  )
import qualified Data.Rewriting.Substitution as S

-- std library any and all rewritten for Reachability results (assuming YES and NO may not appear in the same list)
-- returns YES if some element in the list is YES
-- returns NO if all elements in the list are NO
-- returns MAYBE otherwise
anyR :: [Result f v] -> Result f v
anyR = go False Empty
  where
    go True _ [] = MAYBE
    go False pt [] = NO pt
    go m Empty (NO pt' : xs) = go m pt' xs
    go m pt (NO pt' : xs) = go m (Cons pt pt') xs
    go _ pt (MAYBE : xs) = go True pt xs
    go _ _ (YES x : _) = YES x

allR :: (Ord f, Ord v) => [Result f v] -> Result f v
allR [] = MAYBE
allR xs = foldr andR (YES emptySubst) xs

andR :: (Ord f, Ord v) => Result f v -> Result f v -> Result f v
andR (NO pt) _ = NO pt
-- TODO maybe NO YES would be nice... but in current implementation unsound!
andR (YES s) (YES t) =
  -- FIXME here maybe instead of NO still? :(
  -- TODO No Empty is bad
  maybe
    ( if isMinimal s && isMinimal t
        then NO (Proof ChangeMe (MergeSubst (toSubst s) (toSubst t)))
        else MAYBE
    )
    (\s' -> YES $ compose s' t)
    $ merge s t
andR (YES _) b = b
andR MAYBE MAYBE = MAYBE
andR MAYBE b = andR b MAYBE

-- returns MAYBE for empty list or list with only MAYBE
-- returns YES when first non-MAYBE value is YES
-- returns NO when first non-MAYBE value is NO
firstDefinite :: [Result f v] -> Result f v
firstDefinite [] = MAYBE
firstDefinite (YES x : _) = YES x
firstDefinite (NO pt : _) = NO pt
firstDefinite (MAYBE : xs) = firstDefinite xs

reachUnif :: (Ord f, Ord v) => Substitution f v -> Problem f v -> Result f v
reachUnif s p =
  if isValid s
    then
      maybe
        MAYBE
        (YES . compose s . nonMinimal . fromSubst)
        (unifyRef (src p) (tgt p))
    else MAYBE

collect :: ConnectiveMeta f v -> Result f v -> Result f v
collect (ConnectiveMeta p s rs) (NO pt) =
  NO
    (Collect p s (toList pt ++ getNos rs))
  where
    toList (Cons pt1 pt2) = toList pt1 ++ toList pt2
    toList pt' = [pt']
    getNos = map (\(NO x) -> x) . filter isNo
collect (ConnectiveMeta p s rs) r = MAYBE -- firstDefinite (rs ++ [r])

partitionResults :: [Result f v] -> ([Result f v], [Result f v], [Result f v])
partitionResults = go [] [] []
  where
    go ys ns ms [] = (ys, ns, ms)
    go ys ns ms (m@MAYBE : rs) = go ys ns (m : ms) rs
    go ys ns ms (r@(YES _) : rs) = go (r : ys) ns ms rs
    go ys ns ms (r@(NO _) : rs) = go ys (r : ns) ms rs

collectAny :: ConnectiveMeta f v -> [Result f v] -> Result f v
collectAny (ConnectiveMeta p s rs) rs' = go False [] (rs' ++ rs)
  where
    go True _ [] = MAYBE
    go False pt [] = NO (Collect p s pt)
    go m pt (NO pt' : xs) = go m (pt' : pt) xs
    go _ pt (MAYBE : xs) = go True pt xs
    go _ _ [YES x] = YES x
    go _ _ (YES x : _) = YES (nonMinimal x)

collectAll :: (Ord f, Ord v) => ConnectiveMeta f v -> [Result f v] -> Result f v
collectAll (ConnectiveMeta p s rs) rs' = case allR (rs' ++ rs) of
  NO pt -> NO (Collect p s [pt])
  x -> x

isReachable ::
  (Ord f, Ord v) =>
  [Source f v -> Problem f v -> Result f v] ->
  ProblemType f v ->
  Result f v
isReachable cs (Any m ps) = collectAny m (map (isReachable cs) ps)
isReachable cs (All m ps) = collectAll m (map (isReachable cs) ps)
isReachable _ (Atom p@(Unification u v) (_, pt)) =
  if isUnifiable u v then MAYBE else NO (Proof (source pt) $ Unif p)
isReachable cs (Atom p (s, pt)) =
  firstDefinite (map (\f -> f (source pt) p) (const (reachUnif s) : cs))
-- FIXME I think Im dropping subst and prooftree here (for the output only, otherwise still sound?)
-- case firstDefinite (map (\f -> f p) (reachUnif s : cs)) of
--   YES s' -> trace ("isReach\n\t" ++ show s ++ "\n\t" ++ show s')
--     $ maybe MAYBE fromSubstitution (merge s (fromSubst s'))
--   x -> x
isReachable _ (Bottom (_, pt)) = NO pt
isReachable _ (Top (s, _)) = fromSubstitution s

-- TODO please verify if YES in the above cases is always correct!

isTop :: BoolStruct t u v -> Bool
isTop (Top _) = True
isTop _ = False

isBottom :: BoolStruct t u v -> Bool
isBottom (Bottom _) = True
isBottom _ = False

insertResults :: ConnectiveMeta f v -> [ProblemType f v] -> ConnectiveMeta f v
insertResults (ConnectiveMeta p s rs) ps =
  ConnectiveMeta
    p
    s
    (map toResult ps ++ rs)
  where
    toResult (Top (s, pt)) = YES s
    toResult (Bottom (s, pt)) = NO pt
    toResult _ = MAYBE

fromResult :: Result f v -> ProblemType f v
fromResult MAYBE = undefined
fromResult (YES s) = Top (s, Empty)
fromResult (NO pt) = Bottom (emptySubst, pt)

simplify ::
  (Ord f, Ord v) =>
  [Source f v -> Problem f v -> Result f v] ->
  ProblemType f v ->
  ProblemType f v
simplify _ a@(Any _ []) = a -- fromResult $ collectAny m []
simplify _ a@(All _ []) = a -- fromResult $ collectAll m []
simplify cs a@(All (ConnectiveMeta _ _ []) [x]) = simplify cs x -- TODO losing meta
simplify cs (Any m xs)
  | not $ null yes =
      let m' = insertResults m yes in fromResult $ collectAny m' []
  | otherwise =
      let m' = insertResults m nos
       in if null may then fromResult $ collectAny m' [] else Any m' may
  where
    (yes, ys) = partition isTop $ map (simplify cs) xs
    (nos, may) = partition isBottom ys
simplify cs (All m xs)
  | null nos =
      let m' = insertResults m yes
       in case allR ((collectAll m' []) : (map (const MAYBE) may)) of
            MAYBE -> All m' may
            r -> fromResult r
  | otherwise =
      let m' = insertResults m nos
       in case collectAll m' [] of
            MAYBE -> All m' []
            r -> fromResult r
  where
    (nos, ys) = partition isBottom $ map (simplify cs) xs
    (yes, may) = partition isTop ys
simplify _ p@(Atom unif@(Unification u v) (s', pt)) =
  if isValid s'
    then
      maybe
        (Bottom (emptySubst, Proof (source pt) (Unif unif)))
        ( \s ->
            (top (Proof (source pt) (Unif unif)) . nonMinimal)
              (compose (fromSubst s) s')
        )
        $ unifyRef u v
    else p
simplify cs p@(Atom x (s', pt)) =
  case firstDefinite (map (\f -> f (source pt) x) cs) of
    NO pt' -> Bottom (s', pt') -- TODO verify these cases
    YES s -> Top (s, pt)
    _ -> p
simplify _ p = p

exploit :: (Ord v, Ord f) => ProblemType f v -> ProblemType f v
exploit (All m xs) = All m $ exploit' xs
exploit (Any m xs) = Any m $ map exploit xs
exploit x = x

exploit' :: (Ord v, Ord f) => [ProblemType f v] -> [ProblemType f v]
exploit' = exploitJoinability . exploitMeetability . exploitTransitivity

-- from a list of BoolStruct, collect all Atoms on the top-level
-- this does NOT collect atoms on a lower level eg:
-- stripAtoms [Atom p, Atom q, All [Atom r, Atom s]] = [p, q]
stripAtoms ::
  [ProblemType f v] -> [(Problem f v, Substitution f v, ProofTree f v)]
stripAtoms =
  foldr
    ( \x xs -> case x of
        Atom a@(Reachability _ _) (s, pt) -> (a, s, pt) : xs
        _ -> xs
    )
    []

exploitTransitivity :: (Ord f, Ord v) => [ProblemType f v] -> [ProblemType f v]
exploitTransitivity ps =
  (++ ps)
    $ mapMaybe
      ( \((p1, s1, pt1), (p2, s2, pt2)) ->
          maybe
            Nothing
            ( \s ->
                Just $
                  Atom
                    (Reachability (src p1) (tgt p2))
                    (s, Source (Transitivity p1 p2))
            )
            (merge s1 s2)
      )
      . filter (\((p1, _, _), (p2, _, _)) -> tgt p1 == src p2)
    $ cartProd as as
  where
    as = stripAtoms ps

exploitMeetability :: (Ord f, Ord v) => [ProblemType f v] -> [ProblemType f v]
exploitMeetability ps =
  (++ ps)
    $ mapMaybe
      ( \((p1, s1, pt1), (p2, s2, pt2)) ->
          maybe
            Nothing
            ( \s ->
                Just $
                  Atom
                    (Meetability (tgt p1) (tgt p2) (Just $ src p1))
                    (s, Source (SameLhs p1 p2))
            )
            (merge s1 s2)
      )
      . filter (\((p1, _, _), (p2, _, _)) -> src p1 == src p2)
    $ cartProd' as
  where
    as = stripAtoms ps

exploitJoinability :: (Ord f, Ord v) => [ProblemType f v] -> [ProblemType f v]
exploitJoinability ps =
  (++ ps)
    $ mapMaybe
      ( \((p1, s1, pt1), (p2, s2, pt2)) ->
          maybe
            Nothing
            ( \s ->
                Just $
                  Atom
                    (Joinability (src p1) (src p2) (Just $ tgt p1))
                    (s, Source (SameRhs p1 p2))
            )
            (merge s1 s2)
      )
      . filter (\((p1, _, _), (p2, _, _)) -> tgt p1 == tgt p2)
    $ cartProd' as
  where
    as = stripAtoms ps
