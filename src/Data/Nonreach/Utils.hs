{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.Nonreach.Utils
  ( foldr2,
    nfold,
    cartProd,
    cartProd',
    nubOrd,
    ShowPrettyString (..),
    module Data.Nonreach.Utils.Parse,
  )
where

import Data.Nonreach.Utils.Parse
import qualified Data.Set as Set

-- Because I could not find this function predefined
foldr2 :: ((a, b) -> c -> c) -> [a] -> [b] -> c -> c
foldr2 f xs ys c = foldr f c (zip xs ys)

-- nfold application
nfold :: (Int -> a -> a) -> Int -> (a -> a)
nfold _ 0 = id
nfold f 1 = f 1
nfold f n = nfold f (n - 1) . f n

cartProd :: [a] -> [b] -> [(a, b)]
cartProd xs ys = [(x, y) | x <- xs, y <- ys]

-- cartesian product for list with itself, modulo symmetry
cartProd' :: [a] -> [(a, a)]
cartProd' = go []
  where
    go ys [] = ys
    go ys (x : xs) = go (ys ++ [(x, y) | y <- xs]) xs

-- taken from StackOverflow: TODO source
nubOrd :: Ord a => [a] -> [a]
nubOrd = go Set.empty
  where
    go s (x : xs)
      | Set.member x s = go s xs
      | otherwise = x : go (Set.insert x s) xs
    go _ _ = []

class Show a => ShowPrettyString a where
  showPretty :: a -> String
  showPretty = show

instance ShowPrettyString String where
  showPretty x = showString x []
