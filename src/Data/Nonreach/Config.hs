module Data.Nonreach.Config
  ( Configuration (..),
    Config (..),
    Trs (..),
    Problems (..),
    Check (..),
    Decomposition (..),
    Format (..),
    OutputOption (..),
    Output,
    getFormat,
    parseCtrsInput,
  )
where

import qualified Data.Nonreach.ConditionalRewriting as C
import Data.Nonreach.Problem (Problem (..))

data Trs = TrsFile String | TrsLiteral String

data Problems = ProblemFile String | ProblemLiteral String

data Check = TCAP | STG deriving (Read, Show, Eq)

data Decomposition = NarrowLeft Int deriving (Read, Show)

data Format = Nonreachability | Infeasibility deriving (Read, Show)

data OutputOption = Result | NL | RedundantRules | InfeasibleRules | DroppedRules | InlinedRules | LeftInlinedRules | HumanReadable | Certificate deriving (Read, Show)

type Output = [OutputOption]

data Configuration = Configuration {checks :: [Check], decomps :: [Decomposition], timeout :: Int, fmt :: Maybe Format, redundantRulesCheck :: Bool, output :: Output}

data Config f v = Config {trs :: C.CTRS f v, problems :: [[Problem f v]], format :: Format, fresh :: f, cfg :: Configuration}

parseCtrsInput :: Trs -> IO (C.CTRS String String)
parseCtrsInput (TrsFile file) = C.parseFileIO file
parseCtrsInput (TrsLiteral l) = C.parseIO l
{-# INLINEABLE parseCtrsInput #-}

getFormat :: Trs -> IO Format
getFormat x = do
  c <- parseCtrsInput x
  return $ case C.problems c of
    Nothing -> Nonreachability
    Just _ -> Infeasibility
{-# INLINEABLE getFormat #-}
