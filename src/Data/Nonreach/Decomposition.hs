{-# LANGUAGE FlexibleContexts #-}

module Data.Nonreach.Decomposition
  ( decomp,
    narrowLeft,
  )
where

import Data.Maybe
  ( fromJust,
    isNothing,
    mapMaybe,
  )
import Data.Nonreach.BoolStruct (BoolStruct (..))
import Data.Nonreach.ConditionalRewriting
  ( ConditionalRule (..),
    assumeSatisfied,
    assumeSatisfiedAll,
    fullRewrite,
  )
import Data.Nonreach.Problem
  ( Problem (..),
    ProblemType,
    asTerm,
    atom,
    atom',
    connectiveFromProblem,
    top,
  )
import Data.Nonreach.ProofTree
  ( Method (..),
    ProofTree (..),
    Source (..),
    source,
  )
import Data.Nonreach.Reachability (stripAtoms)
import Data.Nonreach.Substitution
  ( Substitution,
    compose,
    emptySubst,
    emptySubst',
    fromSubst,
    merge',
    toSubst,
  )
import Data.Nonreach.Term
  ( VType,
    equalModuloRenaming,
    isVarDisjoint,
    lift,
  )
import Data.Nonreach.Utils (cartProd)
import Data.Rewriting.Rule
  ( isCollapsing,
    isExpanding,
  )
import Data.Rewriting.Substitution (unifyRef)
import Data.Rewriting.Term
  ( Term (..),
    isFun,
    isLinear,
    vars,
  )

decomp ::
  (Ord f, Ord v) =>
  [Term f v -> Term f v -> Bool] ->
  ProblemType f v ->
  ProblemType f v
decomp cs (Any m ps) = Any m $ map (decomp cs) ps
decomp cs (All m ps) = All m $ map (decomp cs) ps
decomp _ p@(Atom (Reachability (Var _) _) _) = p
decomp _ p@(Atom (Reachability _ (Var _)) _) = p
decomp cs p@(Atom r@(Reachability u@(Fun h us) v@(Fun g vs)) (s, pt))
  | (h /= g) || all (\c -> c u v) cs = p
  | (h == g) && null us = Top (s, Proof RootDecomp (Unif r))
  | otherwise = ps
  where
    ps =
      All (connectiveFromProblem r pt)
        $ map
          ( \(u', v') ->
              decomp cs $ Atom (Reachability u' v') (s, Source RootDecomp)
          )
        $ zip us vs
decomp _ a@(Atom _ _) = a
decomp _ literal = literal

-- drops a path after narrowing, if it requires a problem equal (modulo
-- renaming) to the initial problem
-- TODO currently only works for Meetability, generalize this function
-- dropRepeatedPath
--   :: (Ord v, Ord f)
--   => Term (Maybe f) (VType v)
--   -> Term (Maybe f) (VType v)
--   -> ProblemType f (VType v)
--   -> ProblemType f (VType v)
-- dropRepeatedPath u' v' (Any zs) = Any $ filter go zs
--  where
--   go (All xs) = not $ any (equalModuloRenaming (lift (u', v'))) ys
--    where
--     ys  = map lift $ cartProd us vs
--     us  = filter (equalModuloRenaming u') xs'
--     vs  = filter (equalModuloRenaming v') xs'
--     -- u'  = asTerm (Reachability r u)
--     -- v'  = asTerm (Reachability r v)
--     xs' = map (asTerm . (\(x, _, _) -> x)) $ stripAtoms xs
--   go _ = True
-- dropRepeatedPath _ _ x = x

narrowLeft ::
  (Ord v, Ord f) =>
  (Int -> Term f (VType v) -> Term f (VType v)) ->
  [ConditionalRule f (VType v)] ->
  ProblemType f (VType v) ->
  ProblemType f (VType v)
narrowLeft rn rs (Any m ps) = Any m (map (narrowLeft rn rs) ps)
narrowLeft rn rs (All m ps) = All m (map (narrowLeft rn rs) ps)
narrowLeft rn crs p@(Atom r@(Reachability u v) (s', pt)) =
  if isLinear u && isVarDisjoint u v
    then
      Any ptC $
        Atom (Unification u v) (s', pt')
          : map
            ( \(x, y, cs, s) ->
                All ptC $
                  Atom (Reachability x y) (compose s s', pt')
                    : map (atom' (compose s s', pt')) cs
            )
            (fullRewrite crs rn u v)
    else p
  where
    pt' = Source LNarrowed
    ptC = (connectiveFromProblem r pt)
-- TODO do more with e?
narrowLeft rn crs p@(Atom j@(Joinability u v e) (s', pt)) =
  if isLinear u
    && isLinear v
    && isVarDisjoint u v
    && ( isNothing e
           || let e' = fromJust e in isVarDisjoint u e' && isVarDisjoint v e'
       )
    then
      Any ptC $
        ( All ptC $
            Atom (Unification u v) (s', pt')
              : maybe [] (\z -> [Atom (Reachability u z) (s', pt')]) e
        )
          : map
            ( \(x, y, cs, s) ->
                All ptC $
                  Atom (Joinability x y e) (compose s s', pt')
                    : map (atom' (compose s s', pt')) cs
            )
            (fullRewrite crs rn u v ++ fullRewrite crs rn v u)
    else p
  where
    pt' = Source LNarrowed
    ptC = (connectiveFromProblem j pt)
-- NOTE while it seems sound, it's also useless
-- narrowLeft rn crs p@(Atom m@(Meetability u v e) (s', pt)) =
--   if isLinear u
--        && isLinear v
--        && isVarDisjoint u v
--        && (  isNothing e
--           || let e' = fromJust e in isVarDisjoint u e' && isVarDisjoint v e'
--           )
--        && all (not . isCollapsing . assumeSatisfied) crs
--     then
--       Any ptC
--       $ ( All ptC
--         $ Atom (Unification u v) (s', pt')
--         : maybe [] (\z -> [Atom (Reachability z u) (s', pt')]) e
--         )
--       : map
--           (\(x, y, cs, s) ->
--             All ptC
--               $ Atom (Meetability x y e) (compose s s', pt')
--               : map (atom' (compose s s', pt')) cs
--           )
--           (fullRewrite crs' rn u v ++ fullRewrite crs' rn v u)
--     else p
--  where
--   crs' = map (\(ConditionalRule l r cs) -> ConditionalRule r l cs) crs
--   pt'  = Source LNarrowed
--   ptC  = (connectiveFromProblem m pt)
narrowLeft _ _ a@(Atom _ _) = a
narrowLeft _ _ x = x

-- FIXME currently unsound wrt Reachability results
-- FIXME check where to make the subst non minimal
rootNarrow ::
  (Ord v, Ord f) =>
  [ConditionalRule f (VType v)] ->
  ProblemType f (VType v) ->
  ProblemType f (VType v)
rootNarrow rs bs = go bs
  where
    go (Any m ps) = Any m (map go ps)
    go (All m ps) = All m (map go ps)
    go (Atom (r@(Reachability u@(Fun uf us) v@(Fun vf vs))) (s, pt)) =
      Any
        ptC
        ( ( if uf == vf
              then
                [ Atom (Unification u v) (s, pt'),
                  All ptC $
                    map (\(x, y) -> Atom (Reachability x y) (s, pt')) (zip us vs)
                ]
              else []
          )
            ++ mapMaybe f rs
        )
      where
        pt' = Source LNarrowed
        ptC = (connectiveFromProblem r pt)
        f (ConditionalRule l@(Fun lf ls) r cs) =
          if uf == lf
            then
              Just $
                All ptC $
                  [ Atom (Reachability r v) (s, pt'),
                    Any
                      ptC
                      [ Atom (Unification u l) (s, pt'),
                        All ptC $
                          map (\(x, y) -> Atom (Reachability x y) (s, pt')) (zip us ls)
                      ]
                  ]
                    ++ map (atom' (s, pt')) cs
            else Nothing
    go p = p
