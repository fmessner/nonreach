module Data.Nonreach.Xml
  ( generateXmlCert,
    Proof (..),
    proofFromResult,
  )
where

import Data.Nonreach.ConditionalRewriting
import Data.Nonreach.Problem
import Data.Nonreach.ProofTree
import Data.Nonreach.Reachability
import Data.Nonreach.Utils (ShowPrettyString (..))
import Data.Rewriting.Term
import Data.Version (showVersion)
import Paths_nonreach (version)
import Text.XML.HXT.Arrow.Pickle.Schema
  ( scString1,
  )
import Text.XML.HXT.Arrow.XmlState.RunIOStateArrow
  ( initialSysState,
  )
import Text.XML.HXT.Core

-- helper picklers

xpSeqFlip :: PU a -> PU () -> PU a
xpSeqFlip p = xpWrap (fst, \x -> (x, ())) . xpPair p

xpFixed :: String -> PU ()
xpFixed s =
  ( xpWrapEither
      ( \x ->
          if x == s
            then Right ()
            else
              Left
                ("xpFixed: value " ++ show s ++ " expected, but got " ++ show x),
        const s
      )
      $ xpText
  )
    { theSchema = scString1
    }

xpFixedElem :: String -> String -> PU ()
xpFixedElem e s = xpElem e $ xpFixed s

-- modified version of xpPrim, with no quotations (and no error handling)

xpPrim' :: (Read a, ShowPrettyString a) => PU a
xpPrim' = xpWrapEither (readMaybe, showPretty) xpText
  where
    readMaybe :: Read a => String -> Either String a
    readMaybe str = val (reads str)
      where
        val [(x, "")] = Right x
        val _ = Left $ "xpPrim': reading string " ++ show str ++ " failed"

-- Terms

instance (ShowPrettyString f, Read f, ShowPrettyString v, Read v) => XmlPickler (Term f v) where
  xpickle = xpAlt tag ps
    where
      tag (Var _) = 0
      tag (Fun _ _) = 1
      ps =
        [ xpWrap (Var, \(Var x) -> x) $ xpElem "var" xpPrim',
          xpWrap (uncurry Fun, \(Fun f xs) -> (f, xs)) $
            xpElem "funapp" $
              xpPair
                (xpElem "name" xpPrim')
                (xpList $ xpElem "arg" xpickle)
        ]

-- Problems / Conditions

xpProblemAs ::
  (ShowPrettyString f, Read f, ShowPrettyString v, Read v) =>
  String ->
  PU (Problem f v)
xpProblemAs s =
  xpElem s $
    xpWrap (uncurry Reachability, \c -> (src c, tgt c)) $
      xpPair
        (xpElem "lhs" xpickle)
        (xpElem "rhs" xpickle)

xpCondition ::
  (ShowPrettyString f, Read f, ShowPrettyString v, Read v) =>
  PU (Condition f v)
xpCondition = xpProblemAs "condition"

xpProblemsAsRules ::
  (ShowPrettyString f, Read f, ShowPrettyString v, Read v) =>
  PU [Problem f v]
xpProblemsAsRules = xpElem "rules" $ xpList $ xpProblemAs "rule"

-- Rules

instance (ShowPrettyString f, Read f, ShowPrettyString v, Read v) => XmlPickler (ConditionalRule f v) where
  xpickle =
    xpElem "rule"
      $ xpWrap
        (\(l, r, cs) -> ConditionalRule l r cs, \r -> (lhs r, rhs r, cond r))
      $ xpTriple
        (xpElem "lhs" xpickle)
        (xpElem "rhs" xpickle)
        (xpElem "conditions" $ xpList xpCondition)

xpCRules ::
  (ShowPrettyString f, Read f, ShowPrettyString v, Read v) =>
  PU [ConditionalRule f v]
xpCRules = xpElem "rules" $ xpList xpickle

-- infInput

xpInput ::
  (ShowPrettyString f, Read f, ShowPrettyString v, Read v) =>
  PU ([ConditionalRule f v], [Problem f v])
xpInput =
  xpElem "input" $
    xpElem "infeasibilityInput" $
      xpPair
        xpCRules
        (xpElem "probs" $ xpProblemsAsRules)

-- constants

-- TODO find out correct version

xpCpfVersion :: PU ()
xpCpfVersion = xpFixedElem "cpfVersion" "2.2"

xpProofOrigin :: PU ()
xpProofOrigin =
  xpElem "origin" $
    xpElem "proofOrigin" $
      xpElem "tool" $
        xpWrap (fst, \_ -> ((), ())) $
          xpPair
            (xpFixedElem "name" "nonreach")
            (xpFixedElem "version" (showVersion version))

-- Proof (TODO maybe move out)

data Proof f v
  = GTcap f
  | SameRhss (Problem f v)
  | Trans (Problem f v) (Term f v)
  | Ifrit [ConditionalRule f v] (Proof f v)
  | LeftInline ([ConditionalRule f v], [(ConditionalRule f v, [Condition f v])]) (Proof f v)
  | RightInline ([ConditionalRule f v], [(ConditionalRule f v, [Condition f v])]) (Proof f v)

proofFromResult :: f -> ProofTree f v -> Proof f v
proofFromResult fresh (pt) = let ps = proofs pt in go ps
  where
    go [] = GTcap fresh
    go ((Transitivity (Reachability s t) (Reachability _ u), _) : _) =
      Trans (Reachability s u) t
    go ((SameRhs (Reachability s u) (Reachability t _), _) : _) =
      SameRhss (Joinability s t (Just u))
    go ((_, _) : xs) = go xs
proofFromResult _ _ = undefined

-- inlinedRules

xpInlinedRule ::
  (ShowPrettyString f, Read f, ShowPrettyString v, Read v) =>
  PU (ConditionalRule f v, [Condition f v])
xpInlinedRule =
  xpElem "inlinedRule" $
    xpPair xpickle (xpElem "inlinedConditions" $ xpList xpCondition)

-- certificate

xpProof ::
  (ShowPrettyString f, Read f, ShowPrettyString v, Read v) => PU (Proof f v)
xpProof = xpBaseProof $ xpInfProof
  where
    xpBaseProof = xpElem "proof"
    xpInfProof = xpElem "infeasibilityProof" $ xpAlt tag ps
    tag (GTcap _) = 0
    tag (SameRhss _) = 1
    tag (Trans _ _) = 2
    tag (Ifrit _ _) = 3
    tag (LeftInline _ _) = 4
    tag (RightInline _ _) = 5
    ps =
      [ xpWrap (GTcap, \(GTcap x) -> x) $
          ( xpElem "infeasibleCompoundConditions" $
              (xpElem "name" $ xpPrim')
                `xpSeqFlip` ( xpElem "nonreachabilityProof" $
                                xpFixedElem "nonreachableTcap" ""
                            )
          ),
        xpWrap
          ( \(s, t, u) -> SameRhss (Joinability s t (Just u)),
            \(SameRhss (Joinability s t (Just u))) -> (s, t, u)
          )
          $ ( xpElem "infeasibleRhssEqual" $
                (xpTriple xpickle xpickle xpickle)
                  `xpSeqFlip` ( xpElem "nonjoinabilityProof" $
                                  xpFixedElem "nonjoinableTcap" ""
                              )
            ),
        xpWrap
          ( \(s, t, u) -> Trans (Reachability s u) t,
            \(Trans (Reachability s u) t) -> (s, t, u)
          )
          $ ( xpElem "infeasibleTrans" $
                (xpTriple xpickle xpickle xpickle)
                  `xpSeqFlip` ( xpElem "nonreachabilityProof" $
                                  xpFixedElem "nonreachableTcap" ""
                              )
            ),
        xpWrap (\(rs, p) -> Ifrit rs p, \(Ifrit rs p) -> (rs, p)) $
          (xpElem "ifritRules" $ (xpPair xpCRules xpInfProof)),
        xpWrap
          ( \(rs, rs', p) -> LeftInline (rs, rs') p,
            \(LeftInline (rs, rs') p) -> (rs, rs', p)
          )
          $ ( xpElem "leftInlineConditions" $
                ( xpTriple
                    (xpCRules)
                    (xpElem "inlinedRules" $ xpList $ xpInlinedRule)
                )
                  xpInfProof
            ),
        xpWrap
          ( \(rs, rs', p) -> RightInline (rs, rs') p,
            \(RightInline (rs, rs') p) -> (rs, rs', p)
          )
          $ ( xpElem "rightInlineConditions" $
                ( xpTriple
                    (xpCRules)
                    (xpElem "inlinedRules" $ xpList $ xpInlinedRule)
                )
                  xpInfProof
            )
      ]

xpCert ::
  (ShowPrettyString f, Read f, ShowPrettyString v, Read v) =>
  PU (([ConditionalRule f v], [Problem f v]), Proof f v)
xpCert =
  xpElem "certificationProblem" $
    xpAddFixedAttr "xmlns:xsi" "http://www.w3.org/2001/XMLSchema-instance" $
      xpAddFixedAttr "xsi:noNamespaceSchemaLocation" "../xml/cpf.xsd" $
        xpPair
          (xpInput `xpSeqFlip` xpCpfVersion)
          (xpProof `xpSeqFlip` xpProofOrigin)

-- modified version of writeDocumentToString, since the original restricted output too much

--

writeDoc :: ArrowXml a => SysConfigList -> a XmlTree String
writeDoc config =
  prepareContents (foldr (>>>) id config $ initialSysState) encodeDocument'
    >>> xshow getChildren

generateXmlCert ::
  (ShowPrettyString f, Read f, ShowPrettyString v, Read v) =>
  [ConditionalRule f v] ->
  [Problem f v] ->
  Proof f v ->
  String
generateXmlCert cs ps p =
  let xs = do
        xml <-
          runLA
            ( constA (pickleDoc xpCert ((cs, ps), p))
                >>> writeDoc [withIndent no, withXmlPi yes]
            )
            0
        return xml
   in concat xs
