{-# LANGUAGE TupleSections #-}

module Data.Nonreach.SymbolTransitionGraph
  ( graphFromRules,
    graphFromRulesNonInductive,
    reach,
    rootReachable,
    Graph,
  )
where

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe (fromMaybe)
import Data.Nonreach.BoolStruct (BoolStruct (..))
import Data.Nonreach.ConditionalRewriting
  ( ConditionalRule (..),
    assumeSatisfied,
    fromRule,
    fromRules,
    funsDup,
    split,
  )
import Data.Nonreach.Problem
  ( ConnectiveMeta (..),
    Problem (..),
    atom,
  )
import Data.Nonreach.ProofTree
  ( Method (..),
    ProofTree (..),
    Source (..),
  )
import Data.Nonreach.Reachability
  ( Result (..),
    exploit',
  )
import qualified Data.Nonreach.Reachability as R
import Data.Rewriting.Rule (Rule (..))
import Data.Rewriting.Term (Term (..))
import qualified Data.Rewriting.Term as T
import Data.Set (Set)
import qualified Data.Set as Set

data Graph f = Graph {graph :: Map f (Set f), precs :: Map f (Set f), succs :: Map f (Set f)} deriving (Show)

lookupOrEmpty :: (Ord a) => a -> Map a (Set a) -> Set a
lookupOrEmpty x m = fromMaybe Set.empty $ Map.lookup x m

initGraph :: Ord f => [f] -> Graph f
initGraph xs = Graph e e e where e = Map.fromList $ map (,Set.empty) xs

addEdges :: (Ord f) => Graph f -> f -> [f] -> Graph f
addEdges g n es =
  g
    { graph = Map.adjust (Set.union (Set.fromList es)) n (graph g),
      precs = p,
      succs = s
    }
  where
    rs =
      foldr
        (\x xs -> Set.union xs $ lookupOrEmpty x (succs g))
        (Set.fromList es)
        es
    ls = Set.insert n $ lookupOrEmpty n (precs g)
    p = foldr (Map.adjust (Set.union ls)) (precs g) rs
    s = foldr (Map.adjust (Set.union rs)) (succs g) ls

addRule :: (Ord f, Ord v) => Graph f -> Rule f v -> Graph f
addRule g (Rule l r) = case T.root l of
  Left _ -> case T.root r of
    Left _ -> foldl (\x y -> addEdges x y keys) g keys
    Right i -> foldl (\x y -> addEdges x y [i]) g keys
  Right h -> case T.root r of
    Left _ -> addEdges g h (Map.keys $ graph g)
    Right i -> addEdges g h [i]
  where
    keys = Map.keys $ graph g

graphFromRulesNonInductive ::
  (Ord f, Ord v) => [ConditionalRule f v] -> (Graph f, [ConditionalRule f v])
graphFromRulesNonInductive cs =
  graphFromRules [] (map (fromRule . assumeSatisfied) cs)

graphFromRules ::
  (Ord f, Ord v) =>
  [[ConditionalRule f v] -> Source f v -> Problem f v -> Result f v] ->
  [ConditionalRule f v] ->
  (Graph f, [ConditionalRule f v])
graphFromRules checks crs =
  goC
    (go (initGraph (concatMap funsDup crs)) rs)
    False
    c_rs
    []
    (fromRules rs)
  where
    (rs, c_rs) = split crs
    -- non-conditional rules
    go = foldl addRule
    -- check conditions before adding
    goC g _ [] [] rs' = (g, rs')
    goC g False [] _ rs' = (g, rs')
    goC g True [] cs rs' = goC g False cs [] rs'
    goC g f (c@(ConditionalRule l r xs) : cs) cs' rs' =
      if isNO $
        R.isReachable
          (reach g : map (\x -> x rs') checks)
          -- this Reachability l r is a dummy!
          ( All (ConnectiveMeta (Reachability l r) Initial []) $
              exploit' $
                map
                  atom
                  xs
          )
        then goC g f cs (c : cs') rs'
        else goC (addRule g (Rule l r)) True cs cs' (c : rs')
      where
        isNO (NO _) = True
        isNO _ = False

-- Joinability
children :: (Ord f) => Graph f -> f -> Set f
children g x = lookupOrEmpty x (succs g)

children' :: (Ord f) => Graph f -> f -> Set f
children' g x = Set.insert x $ children g x

isJoinable :: (Ord f) => Graph f -> f -> f -> Bool
isJoinable g u v =
  u == v || (not . null $ Set.intersection (children' g u) (children' g v))

isJoinableAt :: (Ord f) => Graph f -> f -> f -> f -> Bool
isJoinableAt g u v e =
  e `elem` Set.intersection (children' g u) (children' g v)

-- Meetability
parents :: (Ord f) => Graph f -> f -> Set f
parents g x = lookupOrEmpty x (precs g)

parents' :: (Ord f) => Graph f -> f -> Set f
parents' g x = Set.insert x $ parents g x

isMeetable :: (Ord f) => Graph f -> f -> f -> Bool
isMeetable g u v =
  u == v || (not . null $ Set.intersection (parents' g u) (parents' g v))

isMeetableFrom :: (Ord f) => Graph f -> f -> f -> f -> Bool
isMeetableFrom g u v r =
  r `elem` Set.intersection (parents' g u) (parents' g v)

-- Reachability Checks

hasPath :: (Ord f) => Graph f -> f -> f -> Bool
hasPath g s t =
  Map.notMember t (graph g)
    || maybe True (Set.member t) (Map.lookup s (succs g))

rootReachable :: (Ord f) => Graph f -> Term f v -> Term f v -> Bool
rootReachable _ (Var _) _ = True
rootReachable _ _ (Var _) = True
rootReachable g (Fun u _) (Fun v _) = hasPath g u v

isReachable :: (Ord f) => Graph f -> f -> f -> Bool
isReachable g s t = s == t || hasPath g s t

reach :: (Ord f) => Graph f -> Source f v -> Problem f v -> Result f v
-- I see some code duplication but don't know how to get rid of it nicely
reach _ _ (Reachability (Var _) _) = MAYBE
reach _ _ (Reachability _ (Var _)) = MAYBE
reach g src p@(Reachability (Fun u _) (Fun v _)) =
  if isReachable g u v then MAYBE else NO (Proof src $ Stg p)
reach _ _ (Meetability (Var _) _ _) = MAYBE
reach _ _ (Meetability _ (Var _) _) = MAYBE
reach g src p@(Meetability (Fun u _) (Fun v _) (Just (Fun r _))) =
  if isMeetableFrom g u v r then MAYBE else NO (Proof src $ Stg p)
reach g src p@(Meetability (Fun u _) (Fun v _) _) =
  if isMeetable g u v then MAYBE else NO (Proof src $ Stg p)
reach _ _ (Joinability (Var _) _ _) = MAYBE
reach _ _ (Joinability _ (Var _) _) = MAYBE
reach g src p@(Joinability (Fun u _) (Fun v _) (Just (Fun e _))) =
  if isJoinableAt g u v e then MAYBE else NO (Proof src $ Stg p)
reach g src p@(Joinability (Fun u _) (Fun v _) _) =
  if isJoinable g u v then MAYBE else NO (Proof src $ Stg p)
reach _ _ _ = MAYBE -- in case we make up other problems
